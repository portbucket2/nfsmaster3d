﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShakeHandler : MonoBehaviour
{
    public AnimationCurve swingAnim;
    public float value;
    public float level;
    float forwardFac = 1;
    float sideFac = 0;

    public float amp = 5;
    public float ShakeSpeed = 1;
    public float fadingSpeed;

    //public Transform camPosRef;
    //public Transform camPosRefLeft;
    public GameObject upperBody;

    public static CameraShakeHandler instance;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        upperBody = this.gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.V))
        {
            Swing(1,1);
        }

        if (level != 0)
        {
            value += Time.deltaTime * ShakeSpeed;
            if (level > 0)
            {
                level -= Time.deltaTime * fadingSpeed;
            }
            else
            {
                level = 0;
                value = 0;
            }

            if (value > 1)
            {
                value -= 1;
            }
            upperBody.transform.localRotation = Quaternion.Euler(swingAnim.Evaluate(value) * amp * level * forwardFac, swingAnim.Evaluate(value) * amp * level * sideFac, swingAnim.Evaluate(value) * amp * level * sideFac * 0);
        }
    }

    public void Swing(float sideFacc, float ampli)
    {
        amp = ampli;
        level = 1;
        forwardFac = 1;
        if (sideFacc < 0)
        {
            sideFac = -1f;
        }
        else
        {
            sideFac = 1f;
        }

        //sideFac = Mathf.Clamp(sideFacc, -1, 1);

    }
}
