﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemLevelTesting : MonoBehaviour
{
    public Text titleText;
    public Text valueText;
    public string title;
    public float value;
    //public bool isOn;
    public float maxValue;
    public float minValue;
    public float increment;

    public Toggle myToggle;
    // Start is called before the first frame update
    void Start()
    {
        titleText.text = title;
        valueText.text = "" + value;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Add()
    {
        value += increment;
        value = Mathf.Clamp(value, minValue, maxValue);

        valueText.text = "" + value;
    }
    public void Sub()
    {
        value -= increment;
        value = Mathf.Clamp(value, minValue, maxValue);

        valueText.text = "" + value;
    }

    public bool EnabledOrNot()
    {
        return myToggle.isOn;
    }
    public float ValueReturned()
    {
        return value;
    }
}
