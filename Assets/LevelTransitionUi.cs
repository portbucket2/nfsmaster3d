﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelTransitionUi : MonoBehaviour
{
    public Image whiteImage;
    public Color whiteSolid;
    public Color whiteTrans;
    public float whiteValue;
    public float whiteValueTar;
    public Text textLevel;
    [SerializeField]
    bool whitting;
    public AnimationCurve ac;
    float v;
    float speed = 1;
    Color c1;
    Color c2;
    
    // Start is called before the first frame update
    void Start()
    {
        c1 = textLevel.GetComponent<Text>().color;
        Color c = c1;
        c.a = 1;
        c1 = c;
        c.a = 0;
        c2 = c;

        GetComponent<Image>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        //if (whiteValue != whiteValueTar)
        //{
        //    whiteValue = Mathf.MoveTowards(whiteValue, whiteValueTar, Time.deltaTime);
        //    whiteImage.color = Color.Lerp(whiteTrans, whiteSolid, whiteValue);
        //    
        //}
        if (whitting )
        {
            v += Time.deltaTime * speed;
            if(v >= 1)
            {
                v = 0;
                whitting = false;
                GetComponent<Image>().enabled = false;
            }
            whiteValue = ac.Evaluate(v);
            whiteImage.color = Color.Lerp(whiteTrans, whiteSolid, whiteValue);
            textLevel.color = Color.Lerp(c2, c1, whiteValue);


        }
    }

    //public void MakeWhiteValue(float from  ,float v)
    //{
    //    whiteValue = from;
    //    whiteValueTar = v;
    //}
    public void MakeWhiteTransition(float t = 1)
    {
        GetComponent<Image>().enabled = true;
        whitting = true;
        v = 0;
        speed = (1 / t);

        textLevel.text = "LEVEL " + (GameManagementMain.instance.levelIndexDisplayed + 1);
    }
    
}
