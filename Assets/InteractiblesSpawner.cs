﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiblesSpawner : MonoBehaviour
{
    public GameObject waterPipe;
    public GameObject busStation;
    public List<GameObject> interactiblesSpawned;
    public Transform spawnPointLeft;
    public Transform spawnPointRight;
    public int indicator;
    float nextDis;
    public float disThreashold;
    public bool hasWaterPipe;
    public bool hasBusStation;

    public static InteractiblesSpawner instance;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        //nextDis = disThreashold;
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.z > nextDis)
        {
            if (transform.position.z < (LevelValuesAssignManager.instance.finishDistance - 100))
            {
                SpawnInteractible();
            }
                
            nextDis += disThreashold; 
        }
    }

    public void SpawnInteractible()
    {
        int t = Random.Range(0, 2);
        Transform tr = spawnPointLeft;
        float scale = 1;
        GameObject go  ;
        if(t == 0)
        {
            tr = spawnPointLeft;
        }
        else
        {
            tr = spawnPointRight;
            scale = -1;
        }

        if (hasWaterPipe && !hasBusStation)
        {
            if (indicator == 0)
            {
                indicator = 1;
                go =  Instantiate(waterPipe, tr.transform.position, tr.transform.rotation);
                
            }
            else
            {
                go = Instantiate(waterPipe, tr.transform.position, tr.transform.rotation);
                indicator = 0;
            }
        }
        else if (!hasWaterPipe && hasBusStation)
        {
            if (indicator == 0)
            {
                indicator = 1;
                go = Instantiate(busStation, tr.transform.position, tr.transform.rotation);
            }
            else
            {
                go = Instantiate(busStation, tr.transform.position, tr.transform.rotation);
                indicator = 0;
            }
        }
        else if(hasWaterPipe && hasBusStation)
        {
            if (indicator == 0)
            {
                indicator = 1;
                go = Instantiate(waterPipe, tr.transform.position, tr.transform.rotation);
            }
            else
            {
                go = Instantiate(busStation, tr.transform.position, tr.transform.rotation);
                indicator = 0;
            }
        }
        else
        {
            go = null;
        }
        if(go != null)
        {
            go.transform.localScale = new Vector3(go.transform.localScale.x * scale, go.transform.localScale.y, go.transform.localScale.z);

            interactiblesSpawned.Add(go);
            RefreshSpawnedVehicleList();
        }
        
    }

    public void RefreshSpawnedVehicleList()
    {
        List<GameObject> listt = new List<GameObject>();
        for (int i = 0; i < interactiblesSpawned.Count; i++)
        {
            if (interactiblesSpawned[i] != null)
            {
                listt.Add(interactiblesSpawned[i]);
            }
        }
        interactiblesSpawned.Clear();
        interactiblesSpawned = listt;
    }

    public void ResetIt()
    {
        for (int i = 0; i < interactiblesSpawned.Count; i++)
        {

            while(interactiblesSpawned.Count > 0)
            {
                GameObject go = interactiblesSpawned[0];
                interactiblesSpawned.RemoveAt(0);
                Destroy(go);

            }
        }

        nextDis = disThreashold;
    }
}
