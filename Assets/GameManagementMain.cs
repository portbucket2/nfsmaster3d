﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagementMain : MonoBehaviour
{
    public CamSnapSensor camSnapRefEmergency;
    public static GameManagementMain instance;

    public int levelIndexDisplayed;
    public int levelIndex;
    public int maxLevelIndex;
    public bool testingMode;
    private void Awake()
    {
        instance = this;
        if (!testingMode)
        {
            levelIndexDisplayed = PlayerPrefs.GetInt("levelIndexDisplayed", 0);
            
        }
        GetLevelIndexFromLevelIndexDisplayed();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CamSnapEmergency(Vector3 pos)
    {
        camSnapRefEmergency.transform.position = pos;
        camSnapRefEmergency.CamSnapSensorTo(pos);
    }
    public void NextLevelUpdate()
    {
        levelIndexDisplayed += 1;
        PlayerPrefs.SetInt("levelIndexDisplayed", levelIndexDisplayed);
        GetLevelIndexFromLevelIndexDisplayed();
    }
    private void GetLevelIndexFromLevelIndexDisplayed()
    {
        levelIndex = (levelIndexDisplayed ) % (maxLevelIndex + 1);
    }

    public void AnalyticsCallLevelStarted()
    {
        int level = levelIndexDisplayed + 1;
        Debug.Log("LevelStarted " + level);
        LionStudios.Analytics.Events.LevelStarted(level, 0);
    }
    public void AnalyticsCallLevelAccomplished()
    {
        int level = levelIndexDisplayed + 1;
        Debug.Log("LevelAccomplished " + level);
        LionStudios.Analytics.Events.LevelComplete(level, 0);
    }
    public void AnalyticsCallLevelFailed()
    {
        int level = levelIndexDisplayed + 1;
        Debug.Log("LevelFailed " + level);
        LionStudios.Analytics.Events.LevelFailed(level, 0);
    }
}
