﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public GameObject camGame;
    public GameObject camMenu;

    public GameObject MenuSceneObj;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetGameCam()
    {
        MenuSceneObj.SetActive(false);
        camGame.gameObject.SetActive(true);
        camMenu.gameObject.SetActive(false);
    }
    public void GetMenuCam()
    {
        MenuSceneObj.SetActive(true);
        camGame.gameObject.SetActive(false);
        camMenu.gameObject.SetActive(true);
    }
}
