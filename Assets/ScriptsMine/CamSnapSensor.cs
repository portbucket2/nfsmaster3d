﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamSnapSensor : MonoBehaviour
{
    public Transform camPosRef;
    public Transform camPosRefRight;
    public Transform camPosRefLeft;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 10)
        {
            //Debug.Log("GotPlayer");
            CamFollow.instance.SetCameraTempTar(camPosRef);
            
        }

        
    }

    public void CamSnapSensorTo(Vector3 pos)
    {
        if(pos.x > 0)
        {
            CamFollow.instance.SetCameraTempTar(camPosRefLeft);
        }
        else
        {
            CamFollow.instance.SetCameraTempTar(camPosRefRight);
        }
    }
}
