﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestForce : MonoBehaviour
{
    public Vector3 rbVel;
    public Vector3 rbVelLocal;
    public Vector3 rbAngVel;
    public float Engine;
    public float sideFriction;
    public float steerForce;
    public float angVel;
    public float FrontVel;

    Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
        rbVel = rb.velocity;
        rbVelLocal = transform.InverseTransformVector(rb.velocity);
        if(rbVelLocal.z < FrontVel)
        {
            rb.AddForce(transform.forward * Engine);
        }
        rb.AddForce( ( - transform.right*rbVelLocal.x * sideFriction));

        rbAngVel = rb.angularVelocity;
        //rb.velocity = transform.forward * rbVelLocal.z;

        if(angVel > rb.angularVelocity.y)
        {
            rb.AddTorque(transform.up * steerForce* (angVel - rb.angularVelocity.y));
        }

        

    }
}
