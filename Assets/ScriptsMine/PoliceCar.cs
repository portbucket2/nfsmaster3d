﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoliceCar : MonoBehaviour
{
    public float frontVel;
    public float steer;
    public float steerMul;
    public GameObject target;
    public GameObject CarBody;

    public float brakeMul;
    public bool rightMode;
    public float dis;

    public PathPoint pathPointLaneChange;
    public float steerFac;
    
    float accFacTar;
    public CarVisual carVisual;
    public bool isDead;
    public float lastFrontVel;
    public bool pokeMode;
    public float playerFollowOffset;
    public GameObject[] steerWheels;
    float steerEnableMultiplier = 1;
    float accEnableMultiplier = 1;
    public ParticleSystem fireParticle;
    public ParticleSystem explosionParticle;
    bool isNumb;
    // Start is called before the first frame update
    void Start()
    {
        carVisual = target.GetComponent<CarVisual>();
        frontVel = carVisual.carVel * 0.6f ;
        lastFrontVel = frontVel;

        StartCoroutine(pokeModeCoroutine(2f));
    }

    // Update is called once per frame
    void Update()
    {
        
        if (isDead)
        {
            return;
        }
        if (isNumb)
        {
            
            return;
        }
        PoliceCarHeightAdjust();
        RunToTarget();

        //transform.Translate(0, 0, frontVel * Time.deltaTime * 20f * brakeMul);
        transform.Rotate(0, steer * steerEnableMultiplier*Time.deltaTime, 0);

        steerWheels[0].transform.localRotation = Quaternion.Euler(0,steer*0.5f, 0);
        steerWheels[1].transform.localRotation = Quaternion.Euler(0,steer*0.5f, 0);

        CarBody.transform.localRotation = Quaternion.Euler(0, 0,steer * 0.15f);

        if (Input.GetMouseButtonDown(0)){
            DisableSteer();
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            pokeMode = true;
        }
    }
    public void RunToTarget()
    {
        if (target != null)
        {
            //brakeMul = 1;
            float s = transform.InverseTransformPoint(target.transform.position).x * steerMul * frontVel * steerFac * Time.deltaTime *1f;
            steer = Mathf.Lerp(steer, Mathf.Clamp(s, -500f, 500f), Time.deltaTime *20);

            float d = target.transform.position.z - (transform.position.z + playerFollowOffset);
            //float tarVel = (carVisual.carVel + (d *0.5f)) * Time.deltaTime;
            //frontVel = Mathf.Lerp(frontVel, tarVel, Time.deltaTime * 1f);
            
            //frontVel += (-(frontVel - lastFrontVel) * Time.deltaTime*100f);
            if ((target.transform.position.z - transform.position.z) > playerFollowOffset)
            {
                //IncreaseSpeed(1);
                IncreaseSpeed(d * 5f);
            }
            else
            {
                if (pokeMode)
                {
                    IncreaseSpeed(35f);
                }
                else
                {
                    frontVel = Mathf.Lerp(frontVel, carVisual.carVel, Time.deltaTime * 5f);
                    IncreaseSpeed(d *10f);
                }
                if((target.transform.position.z - transform.position.z) <5)
                {
                    //frontVel = carVisual.carVel;
                    //IncreaseSpeed(-100f);
                    frontVel *= 0.99f;
                    steerEnableMultiplier = 0;
                    if (pokeMode)
                    {
                        PokeModDisable();
                        //StartCoroutine(pokeModeCoroutine(2f));

                    }
                    
                    //accEnableMultiplier = Mathf.Lerp(accEnableMultiplier, 0, Time.deltaTime * 5);
                }
                else
                {
                    
                    steerEnableMultiplier = 1;
                    accEnableMultiplier = Mathf.Lerp(accEnableMultiplier, 1, Time.deltaTime * 5);
                }

                //if(Mathf.Abs(frontVel - carVisual.carVel))
                //IncreaseSpeed(-3);
                //frontVel = Mathf.Lerp(frontVel, 0, Time.deltaTime * 0.5f);
            }
            //frontVel = carVisual.carVel * Time.deltaTime;
            //frontVel = transform.InverseTransformPoint(target.transform.position).z;
            //float v = Vector3.Distance(transform.position , target.transform.position)-10;
            //frontVel = Mathf.Lerp(frontVel, v, Time.deltaTime * 5f);
            transform.Translate(0, 0,frontVel * accEnableMultiplier * Time.deltaTime);

            lastFrontVel = frontVel;
            //transform.rotation = Quaternion.LookRotation( target.transform.position - transform.position, Vector3.up);

        }
        else
        {
            //brakeMul = 0;
            //steer = Mathf.Lerp(steer, 0, Time.deltaTime * 5);
        }
        //accFac = Mathf.MoveTowards(accFac, accFacTar, Time.deltaTime * 3);
    }

    public void IncreaseSpeed(float f)
    {
        frontVel +=  f * 0.01f;
        frontVel = Mathf.Clamp(frontVel, 0,100);
    }
    public void DisableSteer()
    {
        steerFac = 0;
        CancelInvoke("EnableSteer");
        Invoke("EnableSteer", 0.4f);

    }
    public void EnableSteer()
    {
        steerFac = 1;
    }

    private void OnCollisionEnter(Collision collision)
    {
        //collision.
        if(collision.gameObject.layer == 8 && !isDead)
        {
            
            if (collision.gameObject.GetComponent<CarOrdinary>())
            {
                collision.gameObject.GetComponent<CarOrdinary>().isDead = true;
                collision.gameObject.GetComponent<CarOrdinary>().fireParticle.Play();
            }
            collision.gameObject.GetComponent<Rigidbody>().AddForce(0,1000, 0);
            collision.gameObject.GetComponent<Rigidbody>().AddTorque(-500, Random.Range(-200,200), Random.Range(-200, 200));
            GameManagementMain.instance.CamSnapEmergency(transform.position);
            
            PoliceCarDestroy();

            //CamFollow.instance.SetCameraTempTar(transform.position);
            
        }
        if (collision.gameObject.layer == 11 && !isDead)
        {

            

            PoliceCarDestroy();



            //CamFollow.instance.SetCameraTempTar(transform.position);

        }

        foreach (ContactPoint item in collision.contacts)
        {
            //item
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 10)
        {
            if(pokeMode == false)
            {
                return;
            }
            other.GetComponentInParent<Rigidbody>().AddForceAtPosition( other.transform.up* 500f + other.transform.forward * 4000f, other.transform.position + (-other.transform.forward*3));
            PlayerHealth.instance.DamagePlayerBy(PlayerHealth.instance.policeHitDamage);
            PlayerHealth.instance.GetComponent<CarPathFollowing>().sparkParticle.transform.position = Vector3.Lerp(PlayerHealth.instance.transform.position, transform.position, 0.5f);
            PlayerHealth.instance.GetComponent<CarPathFollowing>().sparkParticle.Play();
            PokeModDisable();
            frontVel = carVisual.carVel;
            //Debug.Log("Hitted");
            GetComponent<SwingCarBody>().Swing(transform.InverseTransformPoint(target.transform.position).x);

            CameraShakeHandler.instance.Swing(other.gameObject.transform.InverseTransformPoint(transform.position).x, 1.2f);
            //StartCoroutine(pokeModeCoroutine(2f));
        }
    }

    public void PoliceCarDestroy()
    {
        gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
        gameObject.GetComponent<Rigidbody>().AddForce(0, 2000, 500);
        gameObject.GetComponent<Rigidbody>().AddTorque(-1000, Random.Range(-200, 200), Random.Range(-200, 200));
        //Debug.Log(collision.gameObject.name);
        isDead = true;

        EnemySpawner.instance.SpawnEnemy(EnemySpawner.instance.spawnPoliceInterval);
        explosionParticle.Play();
        fireParticle.Play();
        //Destroy(gameObject,4f);
    }

    public IEnumerator pokeModeCoroutine(float t)
    {
        yield return new WaitForSeconds(t);
        PokeModeEnable();
    }
    public void PokeModeEnable()
    {
        pokeMode = true;
        steerFac = Mathf.Lerp(steerFac, 0, Time.deltaTime);
    }
    public void PokeModDisable()
    {
        pokeMode = false;
        steerFac = 1;

        StartCoroutine(pokeModeCoroutine(EnemySpawner.instance.policePokingInterval));
    }

    public void PoliceCarHeightAdjust()
    {
        //float f = 0.2f + Mathf.Clamp((Mathf.Abs(transform.position.x) - 10f), 0, 1) * 0.2f;
        transform.position = new Vector3(transform.position.x, 0.2f, transform.position.z);
    }

    public void MakeMeNumb()
    {
        frontVel = 0;
        accEnableMultiplier = 0;
        isNumb = true;
    }
}
