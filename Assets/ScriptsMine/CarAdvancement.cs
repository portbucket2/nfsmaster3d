﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarAdvancement : MonoBehaviour
{
    public float frontVel;
    public float frontVelMulti;
    CarSteering carSteering;
    
    public float cosVal;
    public float sinVal;
    // Start is called before the first frame update
    void Start()
    {
        carSteering = GetComponent<CarSteering>();
    }

    // Update is called once per frame
    void Update()
    {
        cosVal = Mathf.Cos(carSteering.steerCurrent *Mathf.Deg2Rad);
        sinVal = Mathf.Sin(carSteering.steerCurrent * Mathf.Deg2Rad);
        
        //carSteering.sideVel = frontVel * sinVal * Time.deltaTime * 50f;

        transform.Translate(frontVel * sinVal * Time.deltaTime * 50f, 0, frontVel * cosVal * Time.deltaTime * 50f);
    }
}
