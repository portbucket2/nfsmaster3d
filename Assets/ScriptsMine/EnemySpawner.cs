﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject carPlayer;
    public GameObject carPolice;
    public static EnemySpawner instance;
    public GameObject enemyExisting;
    public float spawnPoliceInterval;
    public float policePokingInterval;

    public bool noPoliceSpawn;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        //SpawnEnemy();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            //SpawnEnemy();
        }
    }
    public void SpawnEnemy()
    {
        if (noPoliceSpawn)
        {
            return;
        }

        if (enemyExisting != null)
        {
            GameObject g = enemyExisting;
            enemyExisting = null;
            Destroy(g, 2f);

        }
        Vector3 spawnPos = carPlayer.transform.position - new Vector3(0, 0,50);
        GameObject go = Instantiate(carPolice, spawnPos, transform.rotation);
        go.GetComponent<PoliceCar>().target = carPlayer;
        enemyExisting = go;
    }
    public void SpawnEnemy(float delay)
    {
        StopAllCoroutines();
        StartCoroutine(SpawnEnemyCoroutine(delay));
    }
    public IEnumerator SpawnEnemyCoroutine(float f)
    {
        yield return new WaitForSeconds(f);
        SpawnEnemy();
    }
    public void Restart()
    {
        Application.LoadLevel(0);
    }

    public void ClearAllEnemies()
    {
        StopAllCoroutines();
        noPoliceSpawn = true;
        if (enemyExisting != null)
        {
            GameObject g = enemyExisting;
            enemyExisting = null;
            g.GetComponent<PoliceCar>().MakeMeNumb();
            Destroy(g,5f);

        }
    }

    public void InitialTasks()
    {
        noPoliceSpawn = false;
        SpawnEnemy();
    }
    public void ResetIt()
    {
        StopAllCoroutines();
        noPoliceSpawn = true;
        if (enemyExisting != null)
        {
            GameObject g = enemyExisting;
            enemyExisting = null;
            g.GetComponent<PoliceCar>().MakeMeNumb();
            Destroy(g);

        }
    }
}
