﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarBodySpringEffect : MonoBehaviour
{
    public GameObject body;
    public PoliceCar policeCar;
    public float ang;
    // Start is called before the first frame update
    void Start()
    {
        policeCar = GetComponent<PoliceCar>();
    }

    // Update is called once per frame
    void Update()
    {
        ang =Mathf.Lerp(ang, policeCar.steer * .15f , Time.deltaTime*5);
        body.transform.localRotation = Quaternion.Euler(new Vector3(0, 0,ang));
    }
}
