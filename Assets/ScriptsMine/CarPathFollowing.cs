﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarPathFollowing : MonoBehaviour
{
    public float frontVel;
    public float steer;
    public float steerMul;
    public PathPoint carTargetPoint;
    public Transform target;
    public float brakeMul;
    public bool rightMode;
    public int currentLaneIndex;
    public float dis;

    //public PathPoint pathPointLaneChange;
    public float accFac;
    float accFacTar;
    public bool withRealCar;
    public Vector3 localTargetMap;
    public float targetAngle;

    public static CarPathFollowing instance;
    public GameObject HeadCanvas;
    public ParticleSystem sparkParticle;
    public ParticleSystem fireParticle;
    public ParticleSystem explosionParticle;
    Vector3 initialPos = new Vector3(0,0.7f, -17);
    private void Awake()
    {
        instance = this;
        initialPos = transform.position;
    }
    // Start is called before the first frame update
    void Start()
    {
        InputMouseSystem.instance.actionSwipeRight += ChangeLaneToRight;
        InputMouseSystem.instance.actionSwipeLeft += ChangeLaneToLeft;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            CarDeathExplosion();
        }
        if (GetComponent<PlayerHealth>().isDead)
        {
            return;
        }
        
        if (!withRealCar)
        {
            transform.Translate(0, 0, frontVel * Time.deltaTime * 20f * brakeMul);
            transform.Rotate(0, steer * Time.deltaTime, 0);
        }
        //if (!carTargetPoint)
        //{
        //    GetInitialTar();
        //}

        RunToTarget();
        SearchForNextTar();
        //if (Input.GetKeyDown(KeyCode.I))
        //{
        //    GetInitialTar();
        //}
        if (Input.GetKeyDown(KeyCode.D))
        {
            ChangeLaneBy(1);
            FollowLane();
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            ChangeLaneBy(-1);
            FollowLane();
        }
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
        {
            //ChangeLane();
            //if (rightMode)
            //{
            //    rightMode = false;
            //    ChangeLaneBy(-1);
            //}
            //else
            //{
            //    rightMode = true;
            //    ChangeLaneBy(1);
            //}
            //if (rightMode)
            //{
            //    //target = carTargetPoint.nextPathPoint.lanePoints[1].transform;
            //    FollowLane();
            //}
            //else
            //{
            //    //target = carTargetPoint.nextPathPoint.lanePoints[0].transform;
            //    FollowLane();
            //}
            accFacTar = 0.5f;
        }
    }

    public void RunToTarget()
    {
        if(carTargetPoint != null)
        {
            brakeMul = 1;
            localTargetMap = transform.InverseTransformPoint(target.transform.position);
            targetAngle = Mathf.Atan2 (localTargetMap.x , localTargetMap.z)* Mathf.Rad2Deg;
            //Debug.Log(t);
            if (Mathf.Abs(targetAngle) > 60)
            {
                if(Mathf.Abs(carTargetPoint.transform.position.z - transform.position.z) < 50)
                {
                    carTargetPoint = carTargetPoint.nextPathPoint;
                    FollowLane();
                }
                
                //if (rightMode)
                //{
                //    //target = carTargetPoint.rightPoint.transform;
                //    FollowLane();
                //}
                //else
                //{
                //    //target = carTargetPoint.leftPoint.transform;
                //    FollowLane();
                //}
                //FollowLane();
                carTargetPoint.ColorIt(currentLaneIndex);
            }
            //Debug.Log(t);
            //float s = transform.InverseTransformPoint(target.transform.position).x * steerMul * frontVel * accFac * Time.deltaTime * 60f;
            float s = targetAngle* steerMul * frontVel * accFac * Time.deltaTime * 100f* 0.2f;
            //if(Mathf.Abs( s) > 60)
            //{
            //    if (rightMode)
            //    {
            //        target = carTargetPoint.nextPathPoint.rightPoint.transform;
            //    }
            //    else
            //    {
            //        target = carTargetPoint.nextPathPoint.leftPoint.transform;
            //    }
            //}

            //steer = Mathf.Lerp(steer,Mathf.Clamp(s, -2000f,2000f), Time.deltaTime * 20);
            steer = Mathf.Lerp(steer,s, Time.deltaTime *60);
            //steer =s;
        }
        else
        {
            brakeMul = 0;
            steer = Mathf.Lerp(steer,0, Time.fixedDeltaTime * 5);
        }
        accFac = Mathf.MoveTowards(accFac, accFacTar, Time.deltaTime * 3);
    }

    public void SearchForNextTar()
    {
        if (carTargetPoint != null)
        {
            dis = Vector3.Distance(transform.position, target.transform.position);
            if ( dis < 15f)
            {
                carTargetPoint.DiscolorIt();
                carTargetPoint = carTargetPoint.nextPathPoint;
                carTargetPoint.ColorIt(currentLaneIndex);
                //if (rightMode)
                //{
                //    //target = carTargetPoint.rightPoint.transform;
                //    FollowLane();
                //}
                //else
                //{
                //    //target = carTargetPoint.leftPoint.transform;
                //    FollowLane();
                //}
                FollowLane();
                accFacTar = 1;
            }
        }
    }
    public void GetInitialTar()
    {
        carTargetPoint = RoadMaker.instance.pathPoints[0];
        //if (rightMode)
        //{
        //    //target = carTargetPoint.rightPoint.transform;
        //    FollowLane();
        //}
        //else
        //{
        //    //target = carTargetPoint.leftPoint.transform;
        //    FollowLane();
        //}
        FollowLane();
        accFacTar = 1;
    }
    public void FollowLane()
    {
        int laneIndex = currentLaneIndex;
        

        target = carTargetPoint.nextPathPoint.lanePoints[laneIndex].transform;

        //float f = carTargetPoint.transform.InverseTransformPoint(transform.position).z;
        //Vector3 curTardis = carTargetPoint.transform.position + new Vector3(0,0,  carTargetPoint.transform.InverseTransformPoint(transform.position).z +15);
        //pathPointLaneChange.transform.position = curTardis;
        //if (rightMode)
        //{
        //    rightMode = false;
        //}
        //else
        //{
        //    rightMode = true;
        //}
        //if(f < 10)
        //{
        //    pathPointLaneChange.nextPathPoint = carTargetPoint.nextPathPoint;
        //    carTargetPoint = pathPointLaneChange;
        //}
        //else
        //{
        //    pathPointLaneChange.nextPathPoint = carTargetPoint.nextPathPoint.nextPathPoint;
        //    carTargetPoint = pathPointLaneChange;
        //}
        //
        //
        //if (rightMode)
        //{
        //    target = carTargetPoint.rightPoint.transform;
        //}
        //else
        //{
        //    target = carTargetPoint.leftPoint.transform;
        //}
    }
    public void ChangeLaneBy(int i)
    {
        int index = currentLaneIndex += i;
        index = Mathf.Clamp(index, 0, 3);
        currentLaneIndex = index;
        
    }

    public void ChangeLaneToRight()
    {
        ChangeLaneBy(1);
        FollowLane();
    }
    public void ChangeLaneToLeft()
    {
        ChangeLaneBy(-1);
        FollowLane();
    }
    public Vector3 GetInitialPos()
    {
        return initialPos;
    }
    public void InitialTasks()
    {
        GetComponent<ForceMover>().InitialTasks();
        transform.position = initialPos;

        GetInitialTar();
        HeadCanvas.SetActive(true);
    }
    public void ResetIt()
    {

        

        steer = 0;
        dis = 0;
        localTargetMap = Vector3.zero;
        targetAngle = 0;

        transform.position = initialPos;
        transform.rotation = Quaternion.Euler(Vector3.zero);

        carTargetPoint = null;
        target = null;

        GetComponent<CapsuleCollider>().enabled = false;
        GetComponent<ForceMover>().springs[0].gameObject.SetActive(true);
        GetComponent<ForceMover>().springs[1].gameObject.SetActive(true);
        GetComponent<ForceMover>().springs[2].gameObject.SetActive(true);
        GetComponent<ForceMover>().springs[3].gameObject.SetActive(true);
        GetComponent<CarPathFollowing>().fireParticle.Stop();

        GetComponent<ForceMover>().ResetIt();
    }

    public void CarDeathExplosion()
    {
        HeadCanvas.SetActive(false);
        //GetComponent<PlayerHealth>().isDead = true;
        GetComponent<CapsuleCollider>().enabled = true;
        GetComponent<Rigidbody>().AddForce(0, 40000, 2000);
        GetComponent<Rigidbody>().AddTorque(10000,Random.Range(-5000 , 5000) , Random.Range(-5000, 5000));

        GetComponent<ForceMover>().springs[0].gameObject.SetActive(false);
        GetComponent<ForceMover>().springs[1].gameObject.SetActive(false);
        GetComponent<ForceMover>().springs[2].gameObject.SetActive(false);
        GetComponent<ForceMover>().springs[3].gameObject.SetActive(false);
    }
    public void CarLevelResultCondition()
    {
        HeadCanvas.SetActive(false);
        if (currentLaneIndex == 0)
        {
            ChangeLaneToRight();
        }
        if (currentLaneIndex == 3)
        {
            ChangeLaneToLeft();
        }
    }
}
