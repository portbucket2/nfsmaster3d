﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputLinker : MonoBehaviour
{
    CarPathFollowing carPathFollowing;
    ForceMover forceMover;
    // Start is called before the first frame update
    void Start()
    {
        forceMover = GetComponent<ForceMover>();
        carPathFollowing = GetComponent<CarPathFollowing>();
    }

    // Update is called once per frame
    void Update()
    {
        forceMover.steerInput = carPathFollowing.steer * 0.001f;
    }
}
