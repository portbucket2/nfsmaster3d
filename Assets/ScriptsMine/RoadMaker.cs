﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadMaker : MonoBehaviour
{
    public RoadUnit roadUnit;
    public int initialsRoadUnits;

    public List<PathPoint> pathPoints;
    public List<RoadUnit> roadUnits;

    public static RoadMaker instance;
    //public CarMovementInputs carMovementInputs;
    public RoadUnit roadUnitFirst;
    public RoadUnit roadUnitLast;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        CreateRoadInitially();
        //carMovementInputs.target = pathPoints[0];
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.V))
        {
            ResetRoadSystem();
        }

        RoadLoopingMaintain();
    }

    public void CreateRoadInitially()
    {
        for (int i = 0; i < initialsRoadUnits; i++)
        {
            Vector3 spawnLocalPos = new Vector3(0, 0, i * roadUnit.length);
            GameObject go =  Instantiate(roadUnit.gameObject, transform.position, transform.rotation);
            go.transform.SetParent(transform);
            go.transform.localPosition = spawnLocalPos;
            roadUnits.Add(go.GetComponent<RoadUnit>());
            for (int j = 0; j < go.GetComponent<RoadUnit>().pathPoints.Count; j++)
            {
                pathPoints.Add(go.GetComponent<RoadUnit>().pathPoints[j]);
            }
        }

        for (int i = 0; i < pathPoints.Count; i++)
        {
            if(i > 0)
            {
                pathPoints[i-1].nextPathPoint = pathPoints[i ];
            }
            
        }

        roadUnitFirst = roadUnits[0];
        roadUnitLast = roadUnits[roadUnits.Count - 1];
    }
    public void FirstRoadUnitRecall()
    {
        GameObject go = roadUnitFirst.gameObject;
        RoadUnit roadU = go.GetComponent<RoadUnit>();
        roadUnits.Remove(roadUnits[0]);
        roadUnits.Add(roadU);

        roadUnitLast.pathPoints[roadUnitLast.pathPoints.Count - 1].nextPathPoint = roadU.pathPoints[0];
        go.transform.localPosition = roadUnitLast.transform.localPosition + new Vector3(0, 0, roadUnit.length);
        roadUnitFirst = roadUnits[0];
        roadUnitLast = roadUnits[roadUnits.Count - 1];


    }

    public void RoadLoopingMaintain()
    {
        if(CarPathFollowing.instance == null)
        {
            return;
        }
        if (!CarPathFollowing.instance.gameObject.activeSelf )
        {
            return;
        }
        float d = CarPathFollowing.instance.transform.position.z - roadUnitFirst.gameObject.transform.position.z;
        if (d > 200)
        {
            FirstRoadUnitRecall();
        }

    }

    public void ResetRoadSystem()
    {
        pathPoints.Clear();
        roadUnits.Clear();

        CreateRoadInitially();
        
    }
}
