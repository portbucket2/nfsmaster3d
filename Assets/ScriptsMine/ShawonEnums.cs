﻿public enum GameStates
{
    MenuIdle    ,
    GameStart   ,
    LevelStart  ,
    GamePlay     ,
    LevelResult ,
    LevelEnd
}

public enum LevelResultType
{
    LevelAccomplished,
    LevelFailed
}
