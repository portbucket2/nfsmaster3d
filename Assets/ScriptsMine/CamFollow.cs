﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFollow : MonoBehaviour
{
    public GameObject target;
    public float sensitivity;
    public Vector3 tempTargetPos;
    public Quaternion tempTargetRot;
    public Transform tempTransform;
    public float tempTargetProgression;
    public bool tempTargetMode;

    public static CamFollow instance;
    public AnimationCurve animC;
    Vector3 tarPos;
    Quaternion tarRot;

    Vector3 offset;
    Quaternion rotoffset;
    [SerializeField]
    float turnTableSnapFac = 5;
    public CamTurntable camTurntable;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        tempTransform = this.gameObject.transform;

        animC = AnimationCurve.EaseInOut(0,0,1,1);
        offset =-target.transform.position + transform.localPosition;
        rotoffset = transform.rotation;

        DisableTempTarMOde(0);
        //Camera.main.transform.localPosition = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        //if (Input.GetKeyDown(KeyCode.A))
        //{
        //    SetCameraTempTar(A.transform);
        //}
        //if (Input.GetKeyDown(KeyCode.B))
        //{
        //    SetCameraTempTar(B.transform);
        //}
        if (camTurntable.turnTableOn)
        {
            if(turnTableSnapFac < 10)
            {
                turnTableSnapFac = Mathf.MoveTowards(turnTableSnapFac, 10, Time.deltaTime * 2f);
            }
            
            //
            ////transform.position = Vector3.Lerp(transform.position, Vector3.Lerp(transform.position, camTurntable.camTurnTableTarget.transform.position, turnTableSnapFac),Time.deltaTime *30);
            //transform.position = Vector3.Lerp(transform.position, camTurntable.camTurnTableTarget.transform.position, animC.Evaluate(turnTableSnapFac));
            //transform.rotation = Quaternion.Lerp(transform.rotation, camTurntable.camTurnTableTarget.transform.rotation, animC.Evaluate(turnTableSnapFac));
            //target = camTurntable.camTurnTableTarget;
            //offset = Vector3.zero;

            transform.position = Vector3.Lerp(transform.position, camTurntable.camTurnTableTarget.transform.position , Time.deltaTime *turnTableSnapFac);
            transform.rotation = Quaternion.Lerp(transform.rotation, camTurntable.camTurnTableTarget.transform.rotation, Time.deltaTime * turnTableSnapFac*0.5f);
            //transform.rotation = Quaternion.LookRotation(target.transform.position - transform.position , Vector3.up);
            //transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(target.transform.position - transform.position, Vector3.up), Time.deltaTime * turnTableSnapFac *2);
            return;
        }

        if (tempTransform == null)
        {
            tempTransform = this.transform;
        }
        if (target)
        {
            transform.position = Vector3.Lerp(transform.position, Vector3.Lerp(target.transform.position + offset, tarPos, animC.Evaluate(tempTargetProgression)), Time.deltaTime * sensitivity);
            transform.rotation = Quaternion.Lerp(rotoffset, tarRot, animC.Evaluate(tempTargetProgression));
        }
        

        if (tempTargetMode)
        {
            tempTargetProgression = Mathf.MoveTowards(tempTargetProgression, 1, Time.deltaTime *1);

            tarPos = Vector3.Lerp(tarPos, tempTransform.position, Time.deltaTime * 5);
            tarRot = Quaternion.Lerp(tarRot, tempTransform.rotation, Time.deltaTime * 5);

        }
        else
        {
            tempTargetProgression = Mathf.MoveTowards(tempTargetProgression, 0, Time.deltaTime * 1);
        }

    }

    public void SetCameraTempTar(Transform temp)
    {
        if (tempTargetMode)
        {
            //tarPos = temp.position;
            //tarRot = temp.rotation;
        }
        else
        {
            tarPos = target.transform.position + offset;
            tarRot = rotoffset;
        }
        

        tempTargetPos = temp.position;
        tempTargetRot = temp.rotation;
        tempTransform = temp;
        EnableTempTarMOde();
    }
    public void EnableTempTarMOde()
    {
        StopAllCoroutines();
        tempTargetMode = true;

        //Invoke("DisableTempTarMOde", 2f);
        DisableTempTarMOde(2);
    }
    public void DisableTempTarMOde(float t = 0)
    {
        StartCoroutine(DisableTempTargetCoroutine(t));
    }
    public IEnumerator DisableTempTargetCoroutine(float t)
    {
        yield return new WaitForSeconds(t);
        tempTargetMode = false;
        
    }
    public void ResetIt()
    {
        camTurntable.turnTableOn = false;
        camTurntable.turntableHolder.transform.localRotation = Quaternion.Euler(Vector3.zero);
        transform.position =target.transform.position + offset;
        transform.rotation =rotoffset;

        turnTableSnapFac = 5;
    }
    public void turnTableEnable()
    {
        camTurntable.turnTableOn = true;
    }
}
