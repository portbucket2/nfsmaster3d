﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSteering : MonoBehaviour
{
    public AnimationCurve steerAnimCurve;
    public float steerAngle;
    public float steerCurrent;
    public float steerTarget;
    public float steerOld;
    public float steerSpeed;
    public GameObject visual;
    //public Vector3 leftLanePos;
    //public Vector3 rightLanePos;
    public float laneWidth;
    public float progression;
    public bool atLeftLane;
    public float settleOffset;
    Quaternion tarRot;
    float targetX;
    public bool settling;
    public bool sideMoving;
    public float sideVel;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        SteerRun();

        if (Input.GetKeyDown(KeyCode.D))
        {
            RightSteer();
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            LeftSteer();
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            SettleSteer();
        }

        DetectSettlePoint();
    }

    public void SteerRun()
    {
        //tarRot = steerAnimCurve
        if (progression < 1)
        {
            progression += Time.deltaTime * steerSpeed;
            
        }
        else
        {
            progression =1;
            sideMoving = false;
            settling = false;
        }
        
        steerCurrent = Mathf.Lerp(steerOld, steerTarget, steerAnimCurve.Evaluate(progression));

        visual.transform.rotation = Quaternion.Euler(new Vector3(0, steerCurrent, 0));

        //transform.Translate(steerCurrent* 0.1f * steerSpeed * Time.deltaTime, 0, 0);
    }

    public void RightSteer()
    {
        steerTarget = steerAngle;
        steerOld = steerCurrent;
        progression = 0;
        atLeftLane = false;
        targetX = laneWidth;
        settling = false;
        
    }
    public void LeftSteer()
    {
        steerTarget = -steerAngle;
        steerOld = steerCurrent;
        progression = 0;
        atLeftLane = true;
        targetX = -laneWidth;
        settling = false;
        
    }

    public void SettleSteer()
    {
        steerTarget = 0;
        steerOld = steerCurrent;
        progression = 0;
    }
    public void DetectSettlePoint()
    {
        if (!settling)
        {
            if (Mathf.Abs(transform.localPosition.x - targetX) < settleOffset)
            {
                SettleSteer();
                settling = true;
            }
        }
        
    }
}
