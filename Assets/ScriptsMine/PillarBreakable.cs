﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PillarBreakable : MonoBehaviour
{
    bool isDead;
    public ParticleSystem sparkParticle;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        //collision.
        if (collision.gameObject.layer == 10 && !isDead)
        {
            gameObject. AddComponent<Rigidbody>();
           
            gameObject.GetComponent<Rigidbody>().AddForce(transform.position.x * 500,100,2000);
            //gameObject.GetComponent<Rigidbody>().AddTorque(-500, Random.Range(-200, 200), Random.Range(-200, 200));

            isDead = true;



            //CamFollow.instance.SetCameraTempTar(transform.position);

        }

        foreach (ContactPoint item in collision.contacts)
        {
            //item
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 10 && !isDead)
        {
            gameObject.GetComponent<Rigidbody>().useGravity = true;
            gameObject.GetComponent<Rigidbody>().AddForce(-transform.position.x *350, 500,200);
            gameObject.GetComponent<Rigidbody>().AddTorque( - 500, Random.Range(-0,0), Random.Range(-500, -500));
            gameObject.AddComponent<BoxCollider>();
            sparkParticle.Play();
        }
    }

}
