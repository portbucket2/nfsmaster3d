﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TruckTimber : MonoBehaviour
{
    public float moveSpeed;
    public float acc = 1;
    public bool isDead;
    public GameObject[] timbers;
    public GameObject upperBody;
    public AnimationCurve swingAnim;
    public float value;
    float levelSide ;
    float forwardFac = 1;
    float sideFac = 1;

    public Transform camPosRef;
    public Transform camPosRefLeft;

    public bool noDamage;
    public ParticleSystem dustParticle;
    public InteractiblesArrow interactiblesArrow;
    // Start is called before the first frame update
    void Start()
    {
        acc = 1;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0, 0, moveSpeed*acc * Time.deltaTime);
        if(levelSide != 0)
        {
            value += Time.deltaTime * 1.5f;
            if (levelSide > 0)
            {
                levelSide -= Time.deltaTime * 0.25f;
            }
            else
            {
                levelSide = 0;
            }

            if (value > 1)
            {
                value -= 1;
            }
            upperBody.transform.localRotation = Quaternion.Euler(swingAnim.Evaluate(value) * 3 *levelSide*forwardFac, 0, swingAnim.Evaluate(value) * 5 * levelSide * sideFac);
        }
        if (isDead)
        {
            moveSpeed = Mathf.Lerp(moveSpeed, 0, Time.deltaTime*0.5f);
            return;
        }
        //transform.Translate(0, 0, moveSpeed * Time.deltaTime);

        if (transform.position.z < (VehicleSpawner.instance.transform.position.z - 500))
        {
            Destroy(gameObject);
        }
        if(transform.position.x < CarPathFollowing.instance.transform.position.x)
        {
            interactiblesArrow.RightArrowMode();
        }
        else
        {
            interactiblesArrow.LeftArrowMode();
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            ReleaseTimbers();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        //collision.
        if (collision.gameObject.layer == 10 && !isDead)
        {
            //isDead = true;
            collision.gameObject.GetComponentInParent<CarPathFollowing>().sparkParticle.transform.position = Vector3.Lerp(collision.gameObject.transform.position, transform.position, 0.5f);
            collision.gameObject.GetComponentInParent<CarPathFollowing>().sparkParticle.Play();

            // //gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            // gameObject.GetComponent<Rigidbody>().AddForce(transform.position.x * 500, 3000, 0);
            // //gameObject.GetComponent<Rigidbody>().AddTorque(500, Random.Range(-200, 200), Random.Range(-200, 200));
            // gameObject.GetComponent<Rigidbody>().AddTorque(500, Random.Range(-200, 200), -transform.position.x * Random.Range(1000, 2000));
            // Destroy(gameObject, 2f);
            Vector3 local = transform.InverseTransformPoint(collision.gameObject.transform.position);
            levelSide = 1;
            if (local.x < 0)
            {
                
                sideFac = -1;
                collision.gameObject.GetComponentInParent<CarPathFollowing>().ChangeLaneToLeft();
            }
            else
            {
                
                sideFac = 1;
                collision.gameObject.GetComponentInParent<CarPathFollowing>().ChangeLaneToRight();
            }
            //if (local.z < 0)
            //{
            //    
            //}
            //else
            //{
            //
            //}
            if (local.z < -8)
            {
                sideFac = 0;

                //collision.gameObject.GetComponentInParent<Rigidbody>().AddForce(sideFac*10000, 0, 0);
                if (!noDamage)
                {
                    PlayerHealth.instance.DamagePlayerBy(PlayerHealth.instance.timberTruchBackHitDamage);
                    noDamage = true;
                    Invoke("DamageOn", 5f);
                }
                
                return;
            }
            
            isDead = true;
            ReleaseTimbers();
            if (local.x < 0)
            {
                CamFollow.instance.SetCameraTempTar(camPosRefLeft);
            }
            else
            {
                CamFollow.instance.SetCameraTempTar(camPosRef);
            }
            





        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 10 && !isDead)
        {
            isDead = true;
            Destroy(gameObject,5f);
            ReleaseTimbers();
            Vector3 local = transform.InverseTransformPoint(other.gameObject.transform.position);
            if(local.x < 0)
            {
                sideFac = -1;
                other.GetComponentInParent<CarPathFollowing>().ChangeLaneToLeft();
            }
            else
            {
                sideFac = 1;
                other.GetComponentInParent<CarPathFollowing>().ChangeLaneToRight();
            }
            
        }
    }
    public void ReleaseTimbers()
    {
        for (int i = 0; i < timbers.Length; i++)
        {
            StartCoroutine(ReleaseCoRoutine(i ,i * 0.02f));
        }
        dustParticle.Play();
        interactiblesArrow.gameObject.SetActive(false);
    }

    public IEnumerator ReleaseCoRoutine( int i , float t)
    {
        yield return new WaitForSeconds(t);

        timbers[i].AddComponent<Rigidbody>();
        Rigidbody rb = timbers[i].GetComponent<Rigidbody>();
        rb.mass = 5;
        float f = Mathf.Clamp(timbers[i].transform.localPosition.x * 0.5f, -1, 1);
        //float f = 1;
        //if (timbers[i].transform.localPosition.x < 0)
        //{
        //    f = -1;
        //}
        rb.AddForce(f * 4000, 50, -2000);
        rb.AddTorque(0, 0, -f * 500);
        timbers[i].transform.SetParent(transform);
    }
    public void DamageOn()
    {
        noDamage = false;
    }
}
