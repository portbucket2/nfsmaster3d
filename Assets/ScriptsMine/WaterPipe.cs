﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterPipe : MonoBehaviour
{
    public GameObject pipeObj;
    public ParticleSystem waterParticle;
    public ParticleSystem sparkParticle;
    public bool gotPolice;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.z < (VehicleSpawner.instance.transform.position.z - 500))
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 10)
        {
            //Debug.Log("GotPlayer");
            
            if(pipeObj.GetComponent<Rigidbody>() == null)
            {
                pipeObj.AddComponent<Rigidbody>();
            }
            
            pipeObj.GetComponent<Rigidbody>().AddForce(0, 1000, 0);
            pipeObj.GetComponent<Rigidbody>().AddTorque(-1500, 0, 0);
            //other.GetComponent<CarPathFollowing>().sparkParticle.transform.position = transform.position;
            sparkParticle.Play();
            waterParticle.Play();

            Destroy(gameObject, 5f);

            if(GetComponentInChildren<InteractiblesArrow>() != null)
            {
                GetComponentInChildren<InteractiblesArrow>().gameObject.SetActive(false);
            }
        }

        if (other.gameObject.layer == 9)
        {
            if (gotPolice)
            {
                return;
            }
            Debug.Log("police");
            if(other.GetComponent<PoliceCar>() != null)
            {
                gotPolice = true;
                other.GetComponent<PoliceCar>().PoliceCarDestroy();
            }
            if (GetComponentInChildren<InteractiblesArrow>() != null)
            {
                GetComponentInChildren<InteractiblesArrow>().gameObject.SetActive(false);
            }
        }
    }
}
