﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMovementInputs : MonoBehaviour
{
    public float steerInput;
    public float steerInputTarget;
    ForceMover forceMover;
    public PathPoint target;

    public float snapDistance;
    public float steerMultiplier;
    // Start is called before the first frame update
    void Start()
    {
        forceMover = GetComponent<ForceMover>();
    }

    // Update is called once per frame
    void Update()
    {
        steerInput = Mathf.MoveTowards(steerInput, steerInputTarget, Time.deltaTime );
        if(forceMover != null)
        {
            forceMover.steerInput = steerInput;
        }
        
        DetermineSteer();

        //NextTargetGrabbing();

    }

    public void DetermineSteer()
    {
        //if (target != null)
        //{
        //    Vector3 targetLocalPos = transform.InverseTransformPoint(target.transform.position);
        //    steerInputTarget = targetLocalPos.x * steerMultiplier;
        //}

        //float steer =Mathf.Clamp( -transform.position.x  , -1f ,1f);
        //steerInputTarget = steer;

    }

    public void NextTargetGrabbing()
    {
        if (target != null)
        {
            if(Vector3.Distance(transform.position, target.transform.position)< snapDistance)
            {
                target = target.GetComponent<PathPoint>().nextPathPoint;
            }
        }
    }
}
