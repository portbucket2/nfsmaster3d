﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleSpawner : MonoBehaviour
{
    public GameObject vehicleToSpawn;
    public GameObject vehicleTimberTruck;
    public GameObject spawnPointLeft;
    public GameObject spawnPointRight;
    public List<GameObject> vehiclesSpawned;
    float spawnIntervalRight;
    float spawnIntervalLeft;
    public float spawnIntervalRightRef;
    public float spawnIntervalLeftRef;
    //public GameObject carPlayer;

    public static VehicleSpawner instance;
    public int countForTimberTruck;
    public int countTimberInterval;
    public bool hasTimberTruck;

    CarPathFollowing carPlayer;

    public bool enableVehicleSpawn;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        carPlayer = ReferenceMaster.instance.carPathFollowing;
  
    }

    // Update is called once per frame
    void Update()
    {

        if (!enableVehicleSpawn)
        {
            return;
        }
        if (transform.position.z > spawnIntervalRight)
        {
            SpawnVehicleRight();
        }
        if (transform.position.z > spawnIntervalLeft)
        {
            SpawnVehicleLeft();
        }
        else
        {
            //spawnInterval -= Time.deltaTime * 200;
        }
        transform.position = new Vector3(0, 0, carPlayer.transform.position.z + 300);
    }

    public void SpawnVehicleRight()
    {
        countForTimberTruck += 1;
        GameObject go ;
        if(countForTimberTruck >= (countTimberInterval+ 1))
        {
            countForTimberTruck = 0;
            if (hasTimberTruck)
            {
                go = Instantiate(vehicleTimberTruck, spawnPointRight.transform.position, spawnPointRight.transform.rotation);
            }
            else
            {
                go = Instantiate(vehicleToSpawn, spawnPointRight.transform.position, spawnPointRight.transform.rotation);
            }
            
        }
        else
        {
            go = Instantiate(vehicleToSpawn, spawnPointRight.transform.position, spawnPointRight.transform.rotation);
        }
        spawnIntervalRight = transform.position.z + ( Random.Range(.8f, 1.2f)* spawnIntervalRightRef);

        vehiclesSpawned.Add(go);
        RefreshSpawnedVehicleList();
        //Instantiate(vehicleToSpawn, spawnPointLeft.transform.position, spawnPointLeft.transform.rotation);
    }

    public void SpawnVehicleLeft()
    {
        GameObject go ;
        spawnIntervalLeft = transform.position.z + (Random.Range(.8f, 1.2f) * spawnIntervalLeftRef);
        //Instantiate(vehicleToSpawn, spawnPointRight.transform.position, spawnPointRight.transform.rotation);
        go = Instantiate(vehicleToSpawn, spawnPointLeft.transform.position, spawnPointLeft.transform.rotation);

        vehiclesSpawned.Add(go);
        RefreshSpawnedVehicleList();
    }

    public void RefreshSpawnedVehicleList()
    {
        List<GameObject> listt = new List<GameObject>();
        for (int i = 0; i < vehiclesSpawned.Count; i++)
        {
            if(vehiclesSpawned[i] != null)
            {
                listt.Add(vehiclesSpawned[i]);
            }
        }
        vehiclesSpawned.Clear();
        vehiclesSpawned = listt;
    }
    public void ResetIt()
    {
        StopVehicleSpawning();
        for (int i = 0; i < vehiclesSpawned.Count; i++)
        {

            while (vehiclesSpawned.Count > 0)
            {
                GameObject go = vehiclesSpawned[0];
                vehiclesSpawned.RemoveAt(0);
                Destroy(go);

            }
        }
        transform.position = new Vector3(0, 0, carPlayer.GetInitialPos().z + 300);
        countForTimberTruck = 0;
    }

    public void InitialTasks()
    {
        transform.position = new Vector3(0, 0, carPlayer.GetInitialPos().z + 300);
        SpawnVehicleRight();
        SpawnVehicleLeft();
        enableVehicleSpawn = true;
        countForTimberTruck = 0;
    }

    public void StopVehicleSpawning()
    {
        enableVehicleSpawn = false;
        //for (int i = 0; i < vehiclesSpawned.Count; i++)
        //{
        //    if (vehiclesSpawned[i] != null)
        //    {
        //        if(vehiclesSpawned[i].GetComponent<CarOrdinary>() != null)
        //        {
        //            vehiclesSpawned[i].GetComponent<CarOrdinary>().acc = 0;
        //        }
        //        
        //    }
        //}
    }
}
