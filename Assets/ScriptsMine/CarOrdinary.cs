﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarOrdinary : MonoBehaviour
{
    public float moveSpeed;
    public float acc = 1;
    public bool isDead;
    public ParticleSystem sparkParticle;
    public ParticleSystem fireParticle;
    // Start is called before the first frame update
    void Start()
    {
        acc = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if (isDead)
        {
            return;
        }
        transform.Translate(0, 0, moveSpeed*acc * Time.deltaTime);

        if(transform.position.z < (VehicleSpawner.instance.transform.position.z - 350))
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        //collision.
        if (collision.gameObject.layer == 10 && !isDead)
        {
            isDead = true;
            
            
            //gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
            gameObject.GetComponent<Rigidbody>().AddForce(transform.position.x*500 ,1000, 0);
            //gameObject.GetComponent<Rigidbody>().AddTorque(500, Random.Range(-200, 200), Random.Range(-200, 200));
            gameObject.GetComponent<Rigidbody>().AddTorque(500, Random.Range(-200, 200), -transform.position.x * Random.Range(1000,2000));
            PlayerHealth.instance.DamagePlayerBy(PlayerHealth.instance.ordinaryCarHitDamage);
            Destroy(gameObject, 2f);

            CameraShakeHandler.instance.Swing(collision.gameObject.transform.InverseTransformPoint(transform.position).x, 0.75f);

            ContactPoint[] item = collision.contacts;
            PlayerHealth.instance.GetComponent<CarPathFollowing>().sparkParticle.transform.position = item[0].point;
            PlayerHealth.instance.GetComponent<CarPathFollowing>().sparkParticle.Play();
            fireParticle.Play();
            //foreach (ContactPoint item in collision.contacts)
            //{
            //    PlayerHealth.instance.GetComponent<CarPathFollowing>(). sparkParticle.transform.position = item.point;
            //    PlayerHealth.instance.GetComponent<CarPathFollowing>().sparkParticle.Play();
            //}

            
           

        }

    }
}
