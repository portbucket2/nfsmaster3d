﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarVisual : MonoBehaviour
{
    public GameObject car;
    //public List<float> fixedTime;
    //public float[] lastTwoFixedSteps;
    //public float currentAdvTime;
    public Vector3[] lastTwoPoses;
    public Vector3[] lastTwoRots;
    public Vector3 pos;
    //public Vector3 pos2;
    public float fac;
    float f;
    public float carVel;
    public Vector3 oldPos;

    
    //public GameObject carFollowRefForPolice;
    // Start is called before the first frame update
    void Start()
    {
        lastTwoPoses[0] = car.transform.position;
        lastTwoRots[0] = Quaternion.ToEulerAngles(car.transform.rotation) * Mathf.Rad2Deg;
        lastTwoPoses[1] = car.transform.position;
        lastTwoRots[1] = Quaternion.ToEulerAngles(car.transform.rotation) * Mathf.Rad2Deg;
        //if (fixedTime.Count == 1)
        //{
        //    //lastTwoFixedSteps[0] = fixedTime[0];
        //    lastTwoPoses[0] = car.transform.position;
        //    lastTwoRots[0] = Quaternion.ToEulerAngles(car.transform.rotation) * Mathf.Rad2Deg;
        //}
        //else if (fixedTime.Count == 2)
        //{
        //    //lastTwoFixedSteps[1] = fixedTime[1];
        //    lastTwoPoses[1] = car.transform.position;
        //    lastTwoRots[1] = Quaternion.ToEulerAngles(car.transform.rotation) * Mathf.Rad2Deg;
        //}

        oldPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (car.GetComponent<PlayerHealth>().isDead)
        {
            transform.position = car.transform.position;
            transform.rotation = car.transform.rotation;
            return;
        }
        fac = Time.time - f;
        pos = LerpAdv(lastTwoPoses[0], lastTwoPoses[1], 1f + (fac/Time.fixedDeltaTime));

        transform.position = pos;
        transform.rotation = Quaternion.Euler(LerpAdv(lastTwoRots[0], lastTwoRots[1], 1f + (fac / Time.fixedDeltaTime)));

        MeasureVel();

        //carFollowRefForPolice.transform.position = new Vector3(transform.position.x, carFollowRefForPolice.transform.position.y, transform.position.z - 15f);
    }
    private void FixedUpdate()
    {
        
        f = Time.time;
        lastTwoPoses[0] = lastTwoPoses[1];
        lastTwoPoses[1] = car.transform.position;

        lastTwoRots[0] = lastTwoRots[1];
        lastTwoRots[1] = Quaternion.ToEulerAngles(car.transform.rotation) * Mathf.Rad2Deg;
        //if(fixedTime.Count < 3)
        //{
        //    fixedTime.Add(Time.time);
        //}
        //else
        //{
        //    fixedTime[0] = fixedTime[1];
        //    fixedTime[1] = fixedTime[2];
        //    fixedTime[2] = Time.time;
        //}
        //
        //if(fixedTime.Count == 1)
        //{
        //    //lastTwoFixedSteps[0] = fixedTime[0];
        //    //lastTwoPoses[0] = car.transform.position;
        //    //lastTwoRots[0] = Quaternion.ToEulerAngles(car.transform.rotation) * Mathf.Rad2Deg;
        //}
        //else if (fixedTime.Count == 2)
        //{
        //    //lastTwoFixedSteps[1] = fixedTime[1];
        //    //lastTwoPoses[1] = car.transform.position;
        //    //lastTwoRots[1] = Quaternion.ToEulerAngles(car.transform.rotation) * Mathf.Rad2Deg;
        //}
        //else
        //{
        //    //lastTwoFixedSteps[1] = fixedTime[fixedTime.Count -1];
        //    //lastTwoFixedSteps[0] = fixedTime[fixedTime.Count - 2];
        //    lastTwoPoses[0] = lastTwoPoses[1];
        //    lastTwoPoses[1] = car.transform.position;
        //
        //    lastTwoRots[0] = lastTwoRots[1];
        //    lastTwoRots[1] = Quaternion.ToEulerAngles(car.transform.rotation) * Mathf.Rad2Deg;
        //}



    }

    public Vector3 LerpAdv(Vector3 from, Vector3 to , float frac)
    {
        
        Vector3 r = from + ((to - from) * frac);
        return r;
    }

    public void MeasureVel()
    {
        carVel = -transform.InverseTransformPoint(oldPos).z / Time.deltaTime;
        oldPos = transform.position;
    }
}
