﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wheelVisual : MonoBehaviour
{
    public GameObject positionHolder;
    public GameObject steerHolder;
    public GameObject rotHolder;
    public GameObject flipHolder;
    public float steerAngle;
    public float rotSpeed;
    public bool right;
    public bool isSteer;
    float f;
    public GameObject posRef;

    public CarPathFollowing carPathFollowing;
    // Start is called before the first frame update
    void Start()
    {
        if (right)
        {
            flipHolder.transform.localRotation = Quaternion.Euler(new Vector3(0, 180, 0));
        }
    }

    // Update is called once per frame
    void Update()
    {
        f += Time.deltaTime * 50 * rotSpeed;
        rotHolder.transform.localRotation = Quaternion.Euler( new Vector3(f, 0, 0));
        steerHolder.transform.localRotation = Quaternion.Euler(new Vector3(0, steerAngle, 0));

        if(carPathFollowing != null)
        {
            if (isSteer)
            {
                //steerAngle = carPathFollowing.steer* 0.15f;
                steerAngle = Mathf.Lerp(steerAngle ,  Mathf.Clamp(carPathFollowing.steer * 0.15f, -40,40), Time.deltaTime*5);
            }
            
        }
        if(posRef!= null)
        {
            positionHolder.transform.localPosition = posRef.transform.localPosition;
        }
    }
}
