﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BusStop : MonoBehaviour
{
    public GameObject[] roofObj;
    public ParticleSystem[] roofParticles;
    public ParticleSystem[] sparkParticles;
    public ParticleSystem waterParticle;
    public bool isdead;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.z < (VehicleSpawner.instance.transform.position.z - 500))
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //if (other.gameObject.layer == 10)
        //{
        //    //Debug.Log("GotPlayer");
        //
        //    if (roofObj.GetComponent<Rigidbody>() == null)
        //    {
        //        roofObj.AddComponent<Rigidbody>();
        //    }
        //
        //    //roofObj.GetComponent<Rigidbody>().AddForce(0, 800, 0);
        //    //roofObj.GetComponent<Rigidbody>().AddTorque(-1000, 0, 0);
        //    waterParticle.Play();
        //}

        if (other.gameObject.layer == 9 || other.gameObject.layer == 10)
        {
            if (isdead)
            {
                return;
            }
            //if (roofObj.GetComponent<Rigidbody>() == null)
            //{
            //    roofObj.AddComponent<Rigidbody>();
            //}
            for (int i = 0; i < roofObj.Length; i++)
            {
                //roofObj[i].GetComponent<Rigidbody>().useGravity = true;
                //roofObj[i].GetComponent<Rigidbody>().AddForce(0, -200, 0);
                //roofObj[i].GetComponent<Rigidbody>().AddTorque(-150, 0, 0);
                StartCoroutine( breakRoof(i, i * 0.1f));
            }
            Destroy(gameObject, 5f);
            //isdead = true;
            //Debug.Log("police");
            if (other.gameObject.layer == 9  && other.GetComponent<PoliceCar>() != null)
            {
                other.GetComponent<PoliceCar>().PoliceCarDestroy();
            }
            if (GetComponentInChildren<InteractiblesArrow>() != null)
            {
                GetComponentInChildren<InteractiblesArrow>().gameObject.SetActive(false);
            }
        }
    }

    public IEnumerator breakRoof(int i , float t)
    {
        yield return new WaitForSeconds(t);
        roofObj[i].GetComponent<Rigidbody>().useGravity = true;
        roofObj[i].GetComponent<Rigidbody>().AddForce(0, -200, 0);
        roofObj[i].GetComponent<Rigidbody>().AddTorque(-150, 0, 0);
        roofParticles[i].Play();
        sparkParticles[i].Play();
    }
}
