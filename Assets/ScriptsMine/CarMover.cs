﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMover : MonoBehaviour
{
    public bool atLeftLane;
    public float laneWidth;
    public float val;
    public float tarVal;
    public float oldVal;
    public float progression;

    public float sideSpeed;
    public Vector3 oldPos;
    public Vector3 tarPos;
    public AnimationCurve sideMoveAnimCurve;
    public AnimationCurve sideMoveAnimCurveinto;
    public bool onProgression;
    // Start is called before the first frame update
    void Start()
    {
        oldPos = tarPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        SidewayMove();

        if (Input.GetKeyDown(KeyCode.D))
        {
            TakeRightLane();
        }
        if (Input.GetKeyDown(KeyCode.A))
        {
            TakeLeftLane();
        }
    }

    public void TakeLeftLane()
    {
        tarVal = -1;
        oldVal = val;
        
        oldPos = transform.position;
        tarPos = new Vector3(laneWidth * tarVal, transform.position.y, transform.position.z);

        if (val == 1 || val == -1)
        {

        }
        else
        {
            if (!onProgression)
            {
                onProgression = true;
            }
        }

    }
    public void TakeRightLane()
    {
        tarVal = 1;
        oldVal = val;
        
        oldPos = transform.position;
        tarPos = new Vector3(laneWidth * tarVal, transform.position.y, transform.position.z);

        if (val == 1 || val == -1)
        {

        }
        else
        {
            if (!onProgression)
            {
                onProgression = true;
            }
        }
    }

    public void SidewayMove()
    {
        val = Mathf.MoveTowards(val, tarVal, Time.deltaTime * sideSpeed);
        if(val == tarVal)
        {
            if (onProgression)
            {
                onProgression = false;
            }
        }
        else
        {
            //if (!onProgression)
            //{
            //    onProgression = true;
            //}
        }   //
        if(tarVal - oldVal != 0)
        {
            progression = Mathf.Abs(val - oldVal) / Mathf.Abs(tarVal - oldVal);
        }
        else
        {
            progression = 0;
        }

        if (onProgression)
        {
            //transform.position = Vector3.Lerp(oldPos, tarPos, sideMoveAnimCurve.Evaluate(progression));
        }
        else
        {
            //transform.position = Vector3.Lerp(oldPos, tarPos, sideMoveAnimCurveinto.Evaluate(progression));
        }
        transform.position = Vector3.Lerp(oldPos, tarPos, sideMoveAnimCurve.Evaluate(progression));

        //float f = sideMoveAnimCurve.

    }
    public float LerpUnBound(float a, float b , float v)
    {
        float d = b - a;
        return a + (d * v);
    }
}
