﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public GameObject level1;
    public static LevelManager instance;
    public LevelResultType levelResultType;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       if( Input.GetKeyDown(KeyCode.R))
       {
            LevelReset();
        }
    }

    public void LevelStartFunction()
    {
       

        LevelManager.instance.level1.SetActive(true);

        ReferenceMaster.instance.carPathFollowing.InitialTasks();
        //LevelValuesAssignManager.instance.GetInitialValues();
        //LevelValuesAssignManager.instance.AssignInitialValue();

        EnemySpawner.instance.InitialTasks();
        VehicleSpawner.instance.InitialTasks();


    }

    public void LevelResultFunction()
    {
        //CarPathFollowing.instance.GetComponent<ForceMover>().BrakeApply();
        CarPathFollowing.instance.CarLevelResultCondition();
        EnemySpawner.instance.ClearAllEnemies();
        VehicleSpawner.instance.StopVehicleSpawning();
        if(levelResultType == LevelResultType.LevelAccomplished)
        {
            CamFollow.instance.turnTableEnable();
            GameManagementMain.instance.AnalyticsCallLevelAccomplished();
        }
        else
        {
            GameManagementMain.instance.AnalyticsCallLevelFailed();
        }
        
        
    }

    public void LevelReset()
    {
        EnemySpawner.instance.ResetIt();
        InteractiblesSpawner.instance.ResetIt();
        VehicleSpawner.instance.ResetIt();
        RoadMaker.instance.ResetRoadSystem();
        ReferenceMaster.instance.carPathFollowing.ResetIt();
        TerrainLoopHandler.instance.ResetIt();

        GameStatesControl.instance.ChangeGameStateTo(GameStates.MenuIdle);
        LevelValuesAssignManager.instance.ResetIt();
        CamFollow.instance.ResetIt();
        PlayerHealth.instance. RefillHealth();


    }
    public void SetLevelResultType(LevelResultType t)
    {
        levelResultType = t;
    }

}
