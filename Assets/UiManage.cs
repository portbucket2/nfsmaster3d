﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManage : MonoBehaviour
{
    public GameObject panelMenuIdle;
    public GameObject panelGameplay;
    public GameObject panelLevelResult;
    public GameObject subPanelLevelSucsess;
    public GameObject subPanelLevelFailure;

    public GameObject panelLevelEnd;
    public GameObject subPanelLevelEndSucsess;
    public GameObject subPanelLevelEndFailure;
    public static UiManage instance;
    public LevelTransitionUi levelTransitionUi;

    public Text levelValue;
    public Text levelValueMenu;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GamePanelAppear()
    {
        panelMenuIdle.SetActive(false);
        panelGameplay.SetActive(true);
        panelLevelResult.SetActive(false);
        panelLevelEnd.SetActive(false);

        levelValue.text = "LEVEL " + (GameManagementMain.instance.levelIndexDisplayed + 1);

    }

    public void MenuIdlePanelAppear()
    {
        panelMenuIdle.SetActive(true);
        panelGameplay.SetActive(false);
        panelLevelResult.SetActive(false);
        panelLevelEnd.SetActive(false);

        levelValueMenu.text = "LEVEL " + (GameManagementMain.instance.levelIndexDisplayed + 1);

    }

    public void LevelResultPanelAppear()
    {
        panelMenuIdle.SetActive(false);
        panelGameplay.SetActive(false);
        panelLevelResult.SetActive(true);
        panelLevelEnd.SetActive(false);
        SubPanelsAppear();
    }

    public void LevelEndPanelAppear()
    {
        panelMenuIdle.SetActive(false);
        panelGameplay.SetActive(false);
        panelLevelResult.SetActive(false);
        panelLevelEnd.SetActive(true);
        SubPanelsAppear();
    }

    public void SubPanelsAppear()
    {
        if(LevelManager.instance.levelResultType == LevelResultType.LevelAccomplished)
        {
            subPanelLevelSucsess.SetActive(true);
            subPanelLevelEndSucsess.SetActive(true);
            subPanelLevelFailure.SetActive(false);
            
            subPanelLevelEndFailure.SetActive(false);

        }
        else
        {
            subPanelLevelSucsess.SetActive(false);
            subPanelLevelEndSucsess.SetActive(false);
            subPanelLevelFailure.SetActive   (true);
            subPanelLevelEndFailure.SetActive(true);
        }
    }

    public void ButtonStartFuntion()
    {
        GameStatesControl.instance.ChangeGameStateTo(GameStates.GameStart, 1);
        levelTransitionUi.MakeWhiteTransition(2);
    }

    public void ButtonNext()
    {
        
        GameManagementMain.instance.NextLevelUpdate();
        LevelManager.instance.LevelReset();
    }
    public void ButtonRetry()
    {

        //GameManagementMain.instance.NextLevelUpdate();
        LevelManager.instance.LevelReset();
    }
}
