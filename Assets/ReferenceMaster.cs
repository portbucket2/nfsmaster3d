﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReferenceMaster : MonoBehaviour
{
    public static ReferenceMaster instance;
    public EnemySpawner enemySpawner;
    public VehicleSpawner vehicleSpawner;
    public CarPathFollowing carPathFollowing;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
