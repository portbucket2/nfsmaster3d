﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class carScript : MonoBehaviour
{

    public Vector3 velocity;
    public Vector3 rbvelocity;
    public Transform center;
    Rigidbody rb;
    public bool go;
    public float speed;
    public float rad;
    public float angvel;
    public GameObject rudder;
    public GameObject rudderLeft;
    public float steer;
    public Vector3 velRight;
    public Vector3 velLeft;
    public Vector3 vel;
    public float angVelApp;

    public WheelCollider[] wc;
    public GameObject[] wcVis;
    public GameObject[] steerWheel;

    public GameObject camHolder;
    public Transform steerCenter;
    public Transform driftCenter;
    float driftMul;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = this.GetComponent<Rigidbody>();
        rad = Vector3.Distance(transform.position, center.position);
        angvel = speed / rad;
        rudder.transform.LookAt(center, transform.up);
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!go)
        {
            rad = Vector3.Distance(transform.position, center.position);
            angvel = speed / rad;
            rudder.transform.LookAt(center, transform.up);
            Vector3 rot = Quaternion.ToEulerAngles(rudder.transform.localRotation) * Mathf.Rad2Deg;
            rudderLeft.transform.localRotation = Quaternion.Euler(new Vector3(rot.x, - rot.y, rot.z));
            velRight = -rudder.transform.right * Time.deltaTime * 60 * speed;
            velLeft = rudderLeft.transform.right * Time.deltaTime * 60 * speed;
            angvel = speed / rad;
            //rudder.transform.rotation = Quaternion.LookRotation(tr);
        }

        //rb.velocity = transform.forward *Time.deltaTime*60* speed ;
        //
        velRight = -rudder.transform.right * Time.deltaTime * 60 * speed;
        velLeft = rudderLeft.transform.right * Time.deltaTime * 60 * speed;
        //vel = velRight;
        vel = Vector3.Lerp(velLeft, velRight, (steer + 1) / 2);
        rb.velocity = new Vector3(vel.x, rb.velocity.y, vel.z);
        
        angvel = speed / rad;
        angVelApp = Mathf.Lerp(-angvel, angvel, (steer + 1) / 2);
        rb.angularVelocity = new Vector3(rb.angularVelocity.x, Time.deltaTime * 60 * angVelApp, rb.angularVelocity.z);
        GetWcOutput();

        camHolder.transform.position = Vector3.Lerp(camHolder.transform.position, transform.position, Time.deltaTime * 10);
        camHolder.transform.rotation = Quaternion.Lerp(camHolder.transform.rotation, transform.rotation, Time.deltaTime * 10);

        rbvelocity = rb.velocity;
        velocity = transform.InverseTransformPoint(rb.velocity);

    }
    private void Update()
    {
        steer = Input.GetAxis("Horizontal");

        if (!Input.GetKey(KeyCode.Space))
        {
            center.transform.position = Vector3.MoveTowards(center.transform.position, steerCenter.transform.position, Time.deltaTime * 5);
            driftMul = Mathf.MoveTowards(driftMul, 1, Time.deltaTime * 5);
        }
        else
        {
            center.transform.position = Vector3.MoveTowards(center.transform.position, driftCenter.transform.position, Time.deltaTime * 10);
            
            driftMul = Mathf.MoveTowards(driftMul, -1, Time.deltaTime * 5);

        }
    }

    void GetWcOutput()
    {
        Vector3 pos;
        Quaternion rot;
        for (int i = 0; i < wc.Length; i++)
        {
            wc[i].GetWorldPose(out pos, out rot);
            wcVis[i].transform.position = pos;

            wcVis[i].transform.Rotate(speed * Time.deltaTime * 60f, 0, 0);
        }
        steerWheel[0].transform.localRotation = Quaternion.Euler(0, steer * 25f* driftMul, 0);
        steerWheel[1].transform.localRotation = Quaternion.Euler(0, steer * 25f* driftMul, 0);
    }
}
