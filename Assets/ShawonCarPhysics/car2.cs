﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class car2 : MonoBehaviour
{
    public Vector3 velocity;
    public Vector3 velocityFor;
    public Vector3 velocityFor2;
    public Transform refR;
    Rigidbody rb;
    public Transform com;
    public Transform forcePoint;
    public WheelCollider[] wc;
    public bool wcBool;
    public float torq;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.centerOfMass = com.transform.localPosition;

        //rb.velocity = transform.forward * 0.1f;
    }

    // Update is called once per frame
    void Update()
    {
        wcBool = wc[0].isGrounded;
        torq = wc[0].brakeTorque;
        rb.AddForce(refR.transform.forward * 1000f);
        if (Input.GetKey(KeyCode.Space))
        {
            //rb.velocity = transform.forward * 0.1f;
            //rb.AddForceAtPosition(transform.forward * 5000f, forcePoint.transform.position);
            for (int i = 0; i < wc.Length; i++)
            {
                //wc[i].motorTorque = 1000;
                //wc[i].brakeTorque = 000;
            }
            
        }
        else
        {
            for (int i = 0; i < wc.Length; i++)
            {
                //wc[i].motorTorque = 000;
                //wc[i].brakeTorque = 10000;
            }
        }
        
        velocity = rb.velocity;

        //velocityFor = transform.InverseTransformVector(velocity);
        velocityFor = refR.transform.InverseTransformVector(velocity);
        velocityFor = new Vector3(velocityFor.x * 0, velocityFor.y, velocityFor.z);

        rb.velocity = transform.TransformVector(velocityFor);
    }
}
