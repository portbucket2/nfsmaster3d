﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MySpring : MonoBehaviour
{
    public LayerMask layer;

    public float springForce;
    public float dampingForce;
    public float springDistance;
    public float appForce;
    public float maxAppForce;
    Rigidbody rb;
    public float oldDis;
    public float newDis;
    public GameObject wheelMesh;
    public GameObject wheelRound;
    public float velFor;
    public float steerAngle;
    public bool steer;
    public bool rightSide;
    public bool isGrounded;

    public GameObject[] springBones;
    public GameObject upperSpringEnd;
    public GameObject lowerSpringEnd;
    float hitDis;
    bool hitted;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponentInParent<Rigidbody>();
        if (rightSide)
        {
            upperSpringEnd.transform.localPosition += new Vector3(-.4f, 0, 0);
            lowerSpringEnd.transform.localPosition += new Vector3(-.4f, 0, 0);
        }
        else
        {
            upperSpringEnd.transform.localPosition += new Vector3(.4f, 0, 0);
            lowerSpringEnd.transform.localPosition += new Vector3(.4f, 0, 0);
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //RaycastHit hit;
        //// Does the ray intersect any objects excluding the player layer
        //if (Physics.Raycast(transform.position, transform.TransformDirection(-Vector3.up), out hit, Mathf.Infinity))
        //{
        //    Debug.DrawRay(transform.position, transform.TransformDirection(-Vector3.up) * hit.distance, Color.yellow);
        //    //wheelMesh.transform.localPosition = new Vector3(0, -(hit.distance - 0.5f), 0);
        //    //Debug.Log("Did Hit" + hit.distance);
        //    hitDis = hit.distance;
        //}
        //else
        //{
        //    Debug.DrawRay(transform.position, transform.TransformDirection(-Vector3.up) * 1000, Color.white);
        //    
        //    //Debug.Log("Did not Hit");
        //}
        if (hitted)
        {
            appForce = (springForce * (-hitDis + springDistance)) - (dampingForce * (-hitDis + oldDis));
        }
        
        appForce = Mathf.Clamp(appForce, -maxAppForce, maxAppForce);
        if (hitDis < (springDistance * 1.5f))
        {
            //rb.AddForce(transform.up * (appForce - dampingForce * (-hit.distance + oldDis)));
            rb.AddForceAtPosition(transform.up * (appForce ), transform.position);
            float tarY = -(hitDis - 0.5f);
            wheelMesh.transform.localPosition = new Vector3(0, Mathf.Lerp(wheelMesh.transform.localPosition.y , tarY, Time.fixedDeltaTime * 20), 0);
            //wheelMesh.transform.localPosition = new Vector3(0,tarY, 0);
            isGrounded = true;
        }
        else
        {
            
            float tarY = -((springDistance * 1.5f) - 1.5f);
            wheelMesh.transform.localPosition = new Vector3(0, Mathf.Lerp(wheelMesh.transform.localPosition.y, tarY, Time.fixedDeltaTime * 5), 0);
            isGrounded = false;
        }
        
        newDis = oldDis = hitDis;

        //rb.AddForce(transform.forward *5* Input.GetAxis("Vertical"));
        //wheelMesh.transform.localPosition = new Vector3(0, -(hit.distance - 0.5f), 0);
        lowerSpringEnd.transform.localPosition = new Vector3(upperSpringEnd.transform.localPosition.x, wheelMesh.transform.localPosition.y, upperSpringEnd.transform.localPosition.z);
        
        if (steer)
        {
            wheelMesh.transform.localRotation = Quaternion.Euler(new Vector3(0, steerAngle, 0));
        }
        //SpringSuspenseVisual();
    }
    private void Update()
    {
        wheelRound.transform.Rotate(velFor * Time.fixedDeltaTime * 110, 0, 0);

        RaycastHit hit;
        // Does the ray intersect any objects excluding the player layer
        if (Physics.Raycast(transform.position, transform.TransformDirection(-Vector3.up), out hit, Mathf.Infinity))
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(-Vector3.up) * hit.distance, Color.yellow);
            //wheelMesh.transform.localPosition = new Vector3(0, -(hit.distance - 0.5f), 0);
            //Debug.Log("Did Hit" + hit.distance);
            hitDis = hit.distance;
            hitted = true;
        }
        else
        {
            Debug.DrawRay(transform.position, transform.TransformDirection(-Vector3.up) * 1000, Color.white);
            hitted =false;
            //Debug.Log("Did not Hit");
        }
    }

    public void SpringSuspenseVisual()
    {
        for (int i = 0; i < springBones.Length; i++)
        {
            float y = Mathf.Lerp(wheelMesh.transform.position.y, upperSpringEnd.transform.position.y, (float)i / springBones.Length);
            //Debug.Log((float)i / springBones.Length);
            springBones[i].transform.position = new Vector3(springBones[i].transform.position.x,y, springBones[i].transform.position.z);
        }
    }
}
