﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceMover : MonoBehaviour
{
    public Vector3 vel;
    public Vector3 angVel;
    Rigidbody rb;
    public GameObject camHolder;

    public JointSpring js;
    public float engineForce;
    public float maxVel;
    public float turnRad;
    public float steerTorq;
    public float sideTiltTorq;

    public MySpring[] springs;
    public float reqAngVel;
    public float steerAngle;
    public bool grounded;
    int groundedCount;

    public bool codedSteer;
    [Range(-1,1)]
    public float steerInput;
    public float accInput;
    float sideMoveFac;
    public float sideMoveFacAtGround;
    public Transform COM;
    [SerializeField]
    bool braked;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }
    // Start is called before the first frame update
    void Start()
    {
        
        rb.centerOfMass = COM.transform.localPosition;

        //BrakeRemove();


    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void FixedUpdate()
    {
        if (GetComponent<PlayerHealth>().isDead)
        {
            vel = Vector3.zero;
            return;
        }

        ManageSprings();
        if (!grounded)
        {
            return;
        }

        vel = transform.InverseTransformVector( rb.velocity);
        if (grounded)
        {
            //vel.x = 0.9f * vel.x;
            sideMoveFac  = Mathf.MoveTowards(sideMoveFac, sideMoveFacAtGround, Time.fixedDeltaTime * 0.25f);
            //vel.x *= sideMoveFac;
            vel.x = angVel.y *vel.z* 0.01f;
            //vel.x = 0.8f * vel.x;
        }
        else
        {
            sideMoveFac = 1;
        }
        
        if( grounded)
        {
            if (vel.z < maxVel)
            {
                float force = 0;
                if (Input.GetKey(KeyCode.Space))
                {
                    force = engineForce * 2f;

                }
                else
                {
                    force = engineForce;

                }
                if (codedSteer)
                {
                    rb.AddForce(transform.forward * force * accInput);
                }
                else
                {
                    rb.AddForce(transform.forward * force * Input.GetAxis("Vertical"));
                }

            }
            else
            {
                rb.AddForce(-transform.forward * engineForce * accInput);
            }

        }
        

        if (grounded)
        {
            if (codedSteer)
            {
                reqAngVel = CalculateAngVel(turnRad, vel.z) * steerInput;
            }
            else
            {
                reqAngVel = CalculateAngVel(turnRad, vel.z) * Input.GetAxis("Horizontal");
            }
            
            
            if(Input.GetAxis("Horizontal") != 0)
            {
                //vel.z = CalculateForVelUsingRadius(turnRad, vel.z);
                //vel.x = CalculateSideVelUsingRadius(turnRad, vel.z) * Input.GetAxis("Horizontal");
            }
            
        }
        
        rb.velocity = transform.forward * vel.z + transform.up * vel.y + transform.right * vel.x ;

        angVel = rb.angularVelocity;


        if (grounded)
        {
            float f = Mathf.Abs(angVel.y - reqAngVel);
            if (angVel.y < reqAngVel)
            {
                //rb.AddTorque(0, steerTorq, 0);
                rb.AddTorque((transform.up *steerTorq*f) + (transform.forward * sideTiltTorq * f));
            }
            else if (angVel.y > reqAngVel)
            {
                //rb.AddTorque(0, -steerTorq, -60);
                rb.AddTorque((transform.up * -steerTorq*f )+ (transform.forward * -sideTiltTorq * f));
            }
        }
        

        //if(Mathf.Abs( angVel.y) < 0.1f)
        //{
        //    angVel.y = 0;
        //}

        rb.angularVelocity = angVel;

        if(camHolder!= null)
        {
            //camHolder.transform.position = Vector3.Lerp(camHolder.transform.position, transform.position, Time.deltaTime * 15);

            //camHolder.transform.rotation = Quaternion.Lerp(camHolder.transform.rotation, transform.rotation, Time.deltaTime * 10);
            Vector3 rot = Quaternion.ToEulerAngles(camHolder.transform.rotation) * Mathf.Rad2Deg;
            //camHolder.transform.rotation = Quaternion.Euler(rot.x * 0, rot.y, rot.z * 0);
            //camHolder.transform.rotation = Quaternion.ToEulerAngles(camHolder.transform.rotation);
            //Debug.Log(Quaternion.ToEulerAngles(camHolder.transform.rotation )*Mathf.Rad2Deg);
        }
        //ForceMode fm = new ForceMode();
        //if (Input.GetKeyDown(KeyCode.B))
        //{
        //    //SpawnEnemy();
        //    BrakeApply();
        //}


    }
    public void ManageSprings()
    {
        groundedCount = 0;
        for (int i = 0; i < springs.Length; i++)
        {
            float radiusMulti = 1.5f / turnRad;
            if (springs[i].rightSide)
            {
                radiusMulti *= -1;
            }
            springs[i].velFor = vel.z * (1 + radiusMulti);
            if (codedSteer)
            {
                springs[i].steerAngle = steerAngle * (1 - radiusMulti) * steerInput;
            }
            else
            {
                springs[i].steerAngle = steerAngle * (1 - radiusMulti) * Input.GetAxis("Horizontal");
            }


            if (springs[i].isGrounded)
            {
                groundedCount += 1;

            }
            if (groundedCount >= 1)
            {
                grounded = true;
            }
            else
            {
                grounded = false;
            }
        }
    }
    public float CalculateForVelUsingRadius(float r, float pararForward)
    {
        float halfLen = 2;
        float angle =( Mathf.Tan(2 / r)) * Mathf.Rad2Deg;
        //Debug.Log("Angle = " + angle);
        float cenRad = r / Mathf.Cos(angle * Mathf.Deg2Rad);
        //Debug.Log("cenRad = " +cenRad);

        float velFor = pararForward * Mathf.Cos(angle * Mathf.Deg2Rad);
        //float velSide = pararForward * Mathf.Cos((90 - angle) * Mathf.Deg2Rad);

        //Debug.Log("velFor = " + velFor + " velside = " + velSide);
        

        return velFor;
    }
    public float CalculateSideVelUsingRadius(float r, float pararForward)
    {
        float halfLen = 2;
        float angle = (Mathf.Tan(2 / r)) * Mathf.Rad2Deg;
        //Debug.Log("Angle = " + angle);
        float cenRad = r / Mathf.Cos(angle * Mathf.Deg2Rad);
        //Debug.Log("cenRad = " + cenRad);

        //float velFor = pararForward * Mathf.Cos(angle * Mathf.Deg2Rad);
        float velSide = pararForward * Mathf.Cos((90 - angle) * Mathf.Deg2Rad);

       //Debug.Log("velFor = " + velFor + " velside = " + velSide);
        

        return velSide;
    }
    public float CalculateAngVel(float r, float pararForward)
    {
        float angVel = pararForward / r;
        return angVel;
    }

    public void BrakeApply()
    {
        braked = true;
        accInput = 0;
        rb.drag = 1;
    }
    public void BrakeRemove()
    {

        braked = false;
        accInput = 1;
        rb.drag = 0;
    }
    public void InitialTasks()
    {
        BrakeRemove();
    }
    public void ResetIt()
    {
        rb.velocity = new Vector3(0, 0, 0);
        rb.angularVelocity = new Vector3(0, 0, 0);
        vel = new Vector3(0, 0, 0);
        angVel = new Vector3(0, 0, 0);
        BrakeApply();
    }
}
