﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmergencyProblemHandler : MonoBehaviour
{
    public GameObject playerObj;
    public Vector3 velocity;
    public Vector3 worldPos;
    Rigidbody rbPlayer;
    public float lowVelCount;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(GameStatesControl.instance.GetCurrentGameState() == GameStates.GamePlay)
        {
            NoStopping();
            NoOutOfWorld();
            if(LevelManager.instance.levelResultType == LevelResultType.LevelAccomplished)
            {
                NoFlipping();
            }
            
        }
        
    }

    public void NoStopping()
    {
        if(playerObj == null)
        {
            return;
        }
        rbPlayer = playerObj.GetComponent<Rigidbody>();
        velocity = rbPlayer.velocity;
        Vector2 vel2 = new Vector2(velocity.x, velocity.z);
        if(vel2.magnitude < 2f)
        {
            lowVelCount += Time.deltaTime;
            if(lowVelCount > 5)
            {
                UiManage.instance.ButtonRetry();
                lowVelCount = 0;
            }
           
        }
        else
        {
            lowVelCount = 0;
        }
    }
    public void NoOutOfWorld()
    {
        if (playerObj == null)
        {
            return;
        }

        if(Mathf.Abs( playerObj.transform.position.x) > 20f)
        {
            UiManage.instance.ButtonRetry();
        }
    }
    public void NoFlipping()
    {
        if (playerObj == null)
        {
            return;
        }

        Vector3 rot = Quaternion.ToEulerAngles(playerObj.transform.rotation) * Mathf.Rad2Deg;
        //Debug.Log(rot);
        float rotLimit = 150f;
        if(Mathf.Abs(rot.x)> rotLimit || Mathf.Abs(rot.y) > rotLimit || Mathf.Abs(rot.z) > rotLimit)
        {
            UiManage.instance.ButtonRetry();
        }
    }
    public void ResetGameLevel()
    {

    }
}
