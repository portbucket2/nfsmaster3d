﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LevelValuesAssignManager : MonoBehaviour
{
    public GameObject finishLineObj;

    public float finishDistance              ;
    public float carOtherInterval               ;
    public bool  timberTruckOn             ;
    public int   timberTruckInterval      ;
    public bool  waterPipeOn          ;
    public bool  busStandOn               ;
    public float roadsideObsInterval    ;
    public float policeSpawnInterval ;
    public float policePokingInterval;
    public float policeDamage;
    public float carDamage;
    public float truckDamage;

    public ItemLevelTesting testfinishDistance     ;
    public ItemLevelTesting testcarOtherInterval   ;
    public ItemLevelTesting testtimberTruckOn      ;
    public ItemLevelTesting testtimberTruckInterval;
    public ItemLevelTesting testwaterPipeOn        ;
    public ItemLevelTesting testbusStandOn         ;
    public ItemLevelTesting testroadsideObsInterval;
    public ItemLevelTesting testpoliceSpawnInterval;
    public ItemLevelTesting testpolicePokingInterval;
    public ItemLevelTesting testpoliceDamage;
    public ItemLevelTesting testcarDamage;
    public ItemLevelTesting testtruckDamage;

    public static LevelValuesAssignManager instance;
    bool levelFinished;
    public EnemySpawner enemySpawner;
    public VehicleSpawner vehicleSpawner;

    public CarPathFollowing carPathFollowing;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        //GetInitialValues();
        //AssignInitialValue();
        //FinishLinePositioning(); 
    }

    // Update is called once per frame
    void Update()
    {
        FinishDetection();
    }

    public void FinishLinePositioning()
    {
        finishLineObj.transform.position = new Vector3(0, 0, finishDistance);
    }
    public void FinishDetection()
    {
        if (levelFinished)
        {
            return;
        }
        if(CarPathFollowing.instance == null)
        {
            return;
        }
        if (CarPathFollowing.instance.transform.position.z > finishDistance)
        {
            levelFinished = true;
            LevelManager.instance.SetLevelResultType(LevelResultType.LevelAccomplished);
            GameStatesControl.instance.ChangeGameStateTo(GameStates.LevelResult);
            
        }
    }

    //public void LevelFinishedFun()
    //{
    //    CarPathFollowing.instance.GetComponent<ForceMover>().BrakeApply();
    //    EnemySpawner.instance.ClearAllEnemies();
    //}

    public void AssignInitialValue()
    {
        FinishLinePositioning();
        vehicleSpawner.spawnIntervalRightRef = carOtherInterval;
        vehicleSpawner.spawnIntervalLeftRef = carOtherInterval*1.25f;
        vehicleSpawner.hasTimberTruck = timberTruckOn;
        vehicleSpawner.countTimberInterval = timberTruckInterval;
        
        vehicleSpawner.GetComponent<InteractiblesSpawner>().hasWaterPipe = waterPipeOn;
        vehicleSpawner.GetComponent<InteractiblesSpawner>().hasBusStation = busStandOn;
        vehicleSpawner.GetComponent<InteractiblesSpawner>().disThreashold = roadsideObsInterval;

        enemySpawner.spawnPoliceInterval =  policeSpawnInterval;
        enemySpawner.policePokingInterval = policePokingInterval;

        carPathFollowing.GetComponent<PlayerHealth>().policeHitDamage =          policeDamage;
        carPathFollowing.GetComponent<PlayerHealth>().ordinaryCarHitDamage=      carDamage;
        carPathFollowing.GetComponent<PlayerHealth>().timberTruchBackHitDamage = truckDamage;

    }
    public void GetInitialValues()
    {
        finishDistance      =      testfinishDistance        .ValueReturned() ;
        carOtherInterval    =      testcarOtherInterval      .ValueReturned()   ;
        timberTruckOn =            testtimberTruckOn         .         EnabledOrNot();
        timberTruckInterval =(int) testtimberTruckInterval   .ValueReturned()  ;
        waterPipeOn         =      testwaterPipeOn           .EnabledOrNot()   ;
        busStandOn          =      testbusStandOn            .EnabledOrNot();  
        roadsideObsInterval =      testroadsideObsInterval   .ValueReturned();
        policeSpawnInterval  =     testpoliceSpawnInterval   . ValueReturned()       ;
        policePokingInterval =     testpolicePokingInterval .ValueReturned();
        policeDamage        =      testpoliceDamage         .ValueReturned()       ;
        carDamage           =      testcarDamage            .ValueReturned()       ;
        truckDamage         =      testtruckDamage          .ValueReturned();
    }

    public void ResetIt()
    {
        levelFinished = false;
        LevelManager.instance.SetLevelResultType(LevelResultType.LevelAccomplished);
    }

    public void GetLevelDataFromDataSet(LevelDataSets.LevelData levelData)
    {
        finishDistance      =      levelData. finishDistance         ;
        carOtherInterval    =      levelData. carOtherInterval       ;
        timberTruckOn =            levelData. timberTruckOn          ;
        timberTruckInterval =(int) levelData. timberTruckInterval    ;
        waterPipeOn         =      levelData. waterPipeOn            ;
        busStandOn          =      levelData. busStandOn             ;
        roadsideObsInterval =      levelData. roadsideObsInterval    ;
        policeSpawnInterval  =     levelData. policeSpawnInterval    ;
        policePokingInterval =     levelData. policePokingInterval   ;
        policeDamage        =      levelData. policeDamage           ;
        carDamage           =      levelData. carDamage              ;
        truckDamage        =       levelData. truckDamage;
    }
    public void LevelDataTransferAtGameStart()
    {
        //GetInitialValues();
        if(GetComponent<LevelDataSets>().levelDataInLevel.Length > GameManagementMain.instance.levelIndex )
        {
            GetLevelDataFromDataSet(GetComponent<LevelDataSets>().levelDataInLevel[GameManagementMain.instance.levelIndex]);
        }
        else
        {
            GetLevelDataFromDataSet(GetComponent<LevelDataSets>().levelDataInLevel[0]);
        }
        
        AssignInitialValue();
    }
}
