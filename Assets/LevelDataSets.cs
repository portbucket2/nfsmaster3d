﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LevelDataSets : MonoBehaviour
{

    public LevelData[] levelDataInLevel;
    LevelValuesAssignManager levelValuesAssignManager;

    [System.Serializable]
    public class LevelData
    {
        public string title;
        public float finishDistance;
        public float carOtherInterval;
        public bool timberTruckOn;
        public int timberTruckInterval;
        public bool waterPipeOn;
        public bool busStandOn;
        public float roadsideObsInterval;
        public float policeSpawnInterval;
        public float policePokingInterval;
        public float policeDamage;
        public float carDamage;
        public float truckDamage;
    }
    // Start is called before the first frame update
    void Start()
    {
        levelValuesAssignManager = GetComponent<LevelValuesAssignManager>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void AssignDataToLevel(int i)
    {
        levelValuesAssignManager .finishDistance        = levelDataInLevel[i].finishDistance              ;
        levelValuesAssignManager .carOtherInterval      = levelDataInLevel[i].carOtherInterval               ;
        levelValuesAssignManager .timberTruckOn         = levelDataInLevel[i].timberTruckOn             ;
        levelValuesAssignManager .timberTruckInterval   = levelDataInLevel[i].timberTruckInterval      ;
        levelValuesAssignManager .waterPipeOn           = levelDataInLevel[i].waterPipeOn          ;
        levelValuesAssignManager .busStandOn            = levelDataInLevel[i].busStandOn               ;
        levelValuesAssignManager .roadsideObsInterval   = levelDataInLevel[i].roadsideObsInterval    ;
        levelValuesAssignManager .policeSpawnInterval   = levelDataInLevel[i].policeSpawnInterval ;
        levelValuesAssignManager .policePokingInterval  = levelDataInLevel[i].policePokingInterval;
        levelValuesAssignManager .policeDamage          = levelDataInLevel[i].policeDamage;
        levelValuesAssignManager .carDamage             = levelDataInLevel[i].carDamage;
        levelValuesAssignManager .truckDamage           = levelDataInLevel[i].truckDamage;
    }
}
