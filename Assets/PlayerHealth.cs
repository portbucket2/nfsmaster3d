﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public float health;
    public float maxHealth = 100;
    //public float healtyh
    public Slider healthSlider;

    public float policeHitDamage;
    public float ordinaryCarHitDamage;
    public float timberTruchBackHitDamage;

    public static PlayerHealth instance;
    public bool isDead;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        health = maxHealth;
        DamagePlayerBy(0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DamagePlayerBy(float h)
    {
        if (isDead)
        {
            return;
        }
        health -= h;
        health = Mathf.Clamp(health, 0, maxHealth);
        healthSlider.value = health * 1.0f;

        if(health == 0)
        {

            PlayerDeath();
            isDead = true;
            PlayerHealth.instance.GetComponent<CarPathFollowing>().fireParticle.Play();
            PlayerHealth.instance.GetComponent<CarPathFollowing>().explosionParticle.Play();
        }
    }
    public void RefillHealth()
    {
        isDead = false;
        health = maxHealth;
        healthSlider.value = health * 1.0f;
    }
    public void PlayerDeath()
    {
        GetComponent<ForceMover>().BrakeApply();
        GetComponent<CarPathFollowing>().CarDeathExplosion();
        LevelManager.instance.SetLevelResultType(LevelResultType.LevelFailed);
        GameStatesControl.instance.ChangeGameStateTo(GameStates.LevelResult, 2f);

    }
}
