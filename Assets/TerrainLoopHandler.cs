﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainLoopHandler : MonoBehaviour
{
    public List<GameObject> terrainLoopObjs;
    public float terrainLength = 400;
    CarPathFollowing carr;
    public static TerrainLoopHandler instance;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        carr = ReferenceMaster.instance.carPathFollowing;
    }

    // Update is called once per frame
    void Update()
    {
        if((carr.transform.position.z - transform.position.z)> terrainLength*1.3f)
        {
            TerrainAdvance();
        }
    }

    public void TerrainLoppIt()
    {
        GameObject go = terrainLoopObjs[0];
        terrainLoopObjs.RemoveAt(0);
        terrainLoopObjs.Add(go);
        go.transform.position += new Vector3(0,0, terrainLength * 3);
    }
    public void TerrainAdvance()
    {
        transform.position += new Vector3(0, 0, terrainLength * 1);
    }
    public void ResetIt()
    {
        transform.position = new Vector3(0, 0,0);
    }
}
