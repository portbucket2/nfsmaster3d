﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamTurntable : MonoBehaviour
{
    public GameObject turntableHolder;
    public GameObject camTurnTableTarget;
    public float rotateSpeed;
    public bool turnTableOn;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (turnTableOn)
        {
            turntableHolder.transform.Rotate(0, rotateSpeed * Time.deltaTime, 0);
        }
    }
}
