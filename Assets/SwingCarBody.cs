﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwingCarBody : MonoBehaviour
{
    public AnimationCurve swingAnim;
    public float value;
    public float level;
    float forwardFac = 1;
    float sideFac = 0;

    public Transform camPosRef;
    public Transform camPosRefLeft;
    public GameObject upperBody;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.V))
        {
            
        }

        if (level != 0)
        {
            value += Time.deltaTime * 1.5f;
            if (level > 0)
            {
                level -= Time.deltaTime * 0.33f;
            }
            else
            {
                level = 0;
            }

            if (value > 1)
            {
                value -= 1;
            }
            upperBody.transform.localRotation = Quaternion.Euler(swingAnim.Evaluate(value) *5 * level * forwardFac, 0, swingAnim.Evaluate(value) * 5 * level * sideFac);
        }
    }

    public void Swing(float sideFacc)
    {
        level = 1;
        forwardFac = 1;
        if(sideFacc > 0)
        {
            sideFac = 1;
        }
        else
        {
            sideFac = -1;
        }

        sideFac = Mathf.Clamp(sideFacc, -1, 1);
        
    }

}
