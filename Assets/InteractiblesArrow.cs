﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiblesArrow : MonoBehaviour
{
    public GameObject upArrow;
    public GameObject rightArrow;
    public GameObject leftArrow;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RightArrowMode()
    {
        upArrow    .SetActive(false);
        rightArrow .SetActive(true);
        leftArrow.SetActive(false);
    }

    public void LeftArrowMode()
    {
        upArrow.SetActive(false);
        rightArrow.SetActive(false);
        leftArrow.SetActive(true);
    }
}
